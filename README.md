# umNETDIF #

umNETDIF is a umnet submodule for UMVIEW project.

Copyright 2014 Raffaele Lovino <raff.lovino at gmail.com>

##Requirements##
* umview-0.8.2.1

## How to install ##
### Before ###
After unpacking **umview**, apply patches to **Makefile.am** and **configure.ac**.

After that, **umnetdif** directory must be copied under umview root directory with this resulting hierarchy
```
#!c

xmview-os/umnetdif
```
### After ###
Configuration file and Makefile must be regenerated using command

```
#!c
$ autoreconf -i

```



Then proceed to compile in the usual way

```
#!c
$ ./configure
$ make
# make install

```