/*test sul numero massimo di file descriptor*/
/*	Ogni processo lanciato all'interno di umview può aprire al più 200 file descriptor.
	Questo perché ad ogni socket virtuale (generic_fd) sono associati:
		-due descriptor di unnamed pipe gestiti dal core di umview;
		-due socket reali (uno per ogni interfaccia di rete);
		-un file regolare che rappresenta la 'key' all'interno della hashtab di umnetdif.
	E' evidente che se un processo può aprire al più 1024 fd, 1024/5 ca uguale a 200*/

#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>

int main()
{
	int sockfd;
	while(1){		
		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if(sockfd == -1) {
			fprintf(stderr, "[%d] ", getpid());
			perror("socket");
			while(1) sleep(1);
			exit(EXIT_FAILURE);
		}	
		printf("[%d] SOCKFD = %d\n", getpid(), sockfd);
		usleep(200);
	}

	

}
