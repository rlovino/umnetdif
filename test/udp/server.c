#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>
#include <netinet/in.h>

#define BUFF_SIZE 2

int main(int argc, char *argv[])
{
	char local_ip[16];
	unsigned short int local_port;
	int retval, sockfd, optval;
	struct sockaddr_in local, foreign;
	socklen_t foreign_len;
	int n;
	char buffer[BUFF_SIZE];
	
	if(argc != 3) {
		fprintf(stderr, "Usage: %s <local_ip> <local_port>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	sprintf(local_ip, "%s", argv[1]);
	local_port = atoi(argv[2]);
	
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd == -1) {
		perror("socket");
		exit(EXIT_FAILURE);
	}
	
	optval = 1;
	retval = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (void*)&optval, sizeof(optval));
	if(retval == -1) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	
	local.sin_family = AF_INET;
	inet_aton(local_ip, &local.sin_addr);
	local.sin_port = htons(local_port);
	retval = bind(sockfd, (struct sockaddr*)&local, sizeof(local));
	if(retval == -1) {
		perror("bind");
		exit(EXIT_FAILURE);
	}
	
	
	while(1) {
		memset(&foreign, 0x00, sizeof(foreign));
		foreign_len = sizeof(foreign);
		n = recvfrom(sockfd, (void*)buffer, BUFF_SIZE, 0, (struct sockaddr*)&foreign, &foreign_len);
		if(n == -1) {
			perror("recvfrom");
			exit(EXIT_FAILURE);
		}
		fprintf(stderr, "Received msg from %s:%d\n", inet_ntoa(foreign.sin_addr), ntohs(foreign.sin_port));
	}
	
	
	
}
