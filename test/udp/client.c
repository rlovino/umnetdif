#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>
#include <netinet/in.h>


int main(int argc, char *argv[])
{
	char source_ip[16], dest_ip[16];
	unsigned short int source_port, dest_port;
	int retval;
	int sockfd;
	int optval;
	int n;
	struct sockaddr_in local, foreign;

	if(argc != 5) {
		fprintf(stderr, "Usage: %s <source_ip> <source_port> <dest_ip> <dest_port>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	
	sprintf(source_ip, "%s", argv[1]);
	sprintf(dest_ip, "%s", argv[3]);
	source_port = atoi(argv[2]);
	dest_port = atoi(argv[4]);
	
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd == -1) {
		perror("socket");
		exit(EXIT_SUCCESS);
	}
	
	optval = 1;
	retval = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (void*)&optval, sizeof(optval));
	if(retval == -1) {
		perror("setsockopt");
		exit(EXIT_SUCCESS);
	}

	local.sin_family = AF_INET;
	inet_aton(source_ip, &local.sin_addr);
	local.sin_port = htons(source_port);
	
	retval = bind(sockfd, (struct sockaddr*)&local, sizeof(local));
	if(retval == -1){
		perror("bind");
		exit(EXIT_FAILURE);
	}
	
	foreign.sin_family = AF_INET;
	inet_aton(dest_ip, &foreign.sin_addr);
	foreign.sin_port = htons(dest_port);
	
	retval = connect(sockfd, (struct sockaddr*)&foreign, sizeof(foreign));
	if(retval == -1){
		perror("connect");
		exit(EXIT_FAILURE);
	}
	
	while(1) {
		n = send(sockfd, "ciao", 5, 0);
		sleep(1);
	}
	
	
	
	
	
}
