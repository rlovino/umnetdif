#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>
#include <netinet/in.h>

#define BUFSIZE 16

struct thread_arg {
	int index;
	char local_ip[16];
	unsigned short int local_port;
};


void *thread_main(void *args)
{
	int retval, sockfd, optval, n;
	struct thread_arg *arg = (struct thread_arg*)args;
	struct sockaddr_in local, foreign;
	socklen_t foreign_len;
	
	printf("thread %d args: local %s:%d\n",
		arg->index, arg->local_ip, arg->local_port);
	
	printf("thread %d: creating socket\n", arg->index);
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd == -1) {
		fprintf(stderr, "thread %d: ERROR socket: ", arg->index);
		perror("");
		pthread_exit(NULL);
	}
	printf("thread %d: created socket %d\n", arg->index, sockfd);
	
	printf("thread %d: setting SO_REUSEADDR option to socket %d\n", arg->index, sockfd);
	optval = 1;
	retval = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (void*)&optval, sizeof(optval));
	if(retval == -1) {
		fprintf(stderr, "thread %d: ERROR setsockopt: ", arg->index);
		perror("");
		pthread_exit(NULL);
	}
	printf("thread %d: option set\n", arg->index);
	
			
	printf("thread %d: binding socket %d\n", arg->index, sockfd);
	local.sin_family = AF_INET;
	inet_aton(arg->local_ip, &local.sin_addr);
	local.sin_port = htons(arg->local_port);
	retval = bind(sockfd, (struct sockaddr*)&local, sizeof(local));
	if(retval == -1) {
		fprintf(stderr, "thread %d: ERROR bind: ", arg->index);
		perror("");
		pthread_exit(NULL);
	}	
	printf("thread %d: socket %d bound\n", arg->index, sockfd);
	
	printf("thread %d: socket %d listening\n", arg->index, sockfd);
	retval = listen(sockfd, 10);
	if(retval == -1) {
		fprintf(stderr, "thread %d: ERROR listen: ", arg->index);
		perror("");
		pthread_exit(NULL);
	}	
		
	printf("thread %d: accepting connection\n", arg->index);
	foreign_len = sizeof(foreign);
	retval = accept(sockfd, (struct sockaddr*)&foreign, &foreign_len);
	if(retval == -1) {
		fprintf(stderr, "thread %d: ERROR accept: ", arg->index);
		perror("");
		pthread_exit(NULL);
	}	
	printf("thread %d: accepted connection from %s:%d\n", arg->index, inet_ntoa(foreign.sin_addr), ntohs(foreign.sin_port));

	
	while(1) sleep(1);
	
	pthread_exit(NULL);
}


int main(int argc, char *argv[])
{
	int rc;
	pthread_t thread[2];
	pthread_attr_t attr;
	int i;
	struct thread_arg args[2];
	int timeout;

	if(argc != 6) {
		fprintf(stderr, "Usage: %s <local_ip1> <local_port1> <local_ip2> <local_port2> <timeout millisec>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	
	timeout = atoi(argv[5]);
	fprintf(stderr, "Timeout set to %d ms\n", timeout);
	
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	
	
	for(i=0; i<2; i++) {
		args[i].index = i;
		sprintf(args[i].local_ip, "%s", argv[1+2*i]);
		args[i].local_port = atoi(argv[2+2*i]);
		
		printf("creating thread %d\n", i);
		rc = pthread_create(&thread[i], &attr, thread_main, (void*)&args[i]);
		if(rc == -1) {
			perror("pthread_create");
			exit(EXIT_FAILURE);
		}
		
		if(timeout) usleep(timeout);
	}
	
	pthread_attr_destroy(&attr);
	for(i=0; i<2; i++) {
		rc = pthread_join(thread[i], NULL);
		if(rc == -1) {
			perror("pthread_join");
			exit(EXIT_FAILURE);
		}
	}
	printf("CIAO\n");
	pthread_exit(NULL);	
}
