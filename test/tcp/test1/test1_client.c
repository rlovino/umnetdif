/*test connessione doppia a singolo host*/
/*	lato client: lanciare due istanze di questo client*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <pthread.h>
#include <poll.h>
#include <stdint.h>
#include <arpa/inet.h>

int main(int argc, char *argv[]) {

	char src_addr[16];
	char dst_addr[16];
	uint16_t src_port;
	uint16_t dst_port;

	if(argc != 5) {
		fprintf(stderr, "Usage: %s <ip_src_addr> <src_port_no> <ip_dst_addr> <src_dst_no>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	else {
		sprintf(src_addr, "%s", argv[1]);
		src_port = atoi(argv[2]);
		sprintf(dst_addr, "%s", argv[3]);
		dst_port = atoi(argv[4]);
	}
	printf("SOURCE %s:%d\n", src_addr, src_port);
	printf("DESTINATION %s:%d\n", dst_addr, dst_port);

	int sockfd;
	int retval;
	int optval;
	struct sockaddr_in local, foreign;
	
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd == -1) {
		perror("socket");
		exit(EXIT_FAILURE);
	}
	
	optval = 1;
	retval = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (void*)&optval, sizeof(optval));
	if(retval == -1) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	
	local.sin_family = AF_INET;
	local.sin_addr.s_addr = inet_addr(src_addr);
	local.sin_port = htons(src_port);
	retval = bind(sockfd, (struct sockaddr*)&local, sizeof(struct sockaddr_in));
	if(retval == -1) {
		perror("bind");
		exit(EXIT_FAILURE);
	}
	
	foreign.sin_family = AF_INET;
	foreign.sin_addr.s_addr = inet_addr(dst_addr);
	foreign.sin_port = htons(dst_port);
	retval = connect(sockfd, (struct sockaddr*)&foreign, sizeof(struct sockaddr_in));
	if(retval == -1) {
		perror("connect");
		exit(EXIT_FAILURE);
	}
	
	while(1) sleep(1);
	
	
}
