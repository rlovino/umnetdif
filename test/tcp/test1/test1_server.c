/*test connessione doppia a singolo host*/
/*	lato server da lanciare in umview: resta in attesa di due connessioni tcp
	le connessioni devono partire dallo stesso host (stesso indirizzo ip)*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <pthread.h>
#include <poll.h>
#include <stdint.h>
#include <arpa/inet.h>

int main(int argc, char *argv[]) {

	char addr[16];
	uint16_t port;

	if(argc != 3) {
		fprintf(stderr, "Usage: %s <ip_addr> <port_no>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	else {
		sprintf(addr, "%s", argv[1]);
		port = atoi(argv[2]);
	}
	printf("SOCKET BOUND TO %s:%d\n", addr, port);

	
	/**/
	int sockfd, newsock[2];
	int retval;
	int optval;
	int i;
	struct sockaddr_in local, foreign[2];
	socklen_t foreign_len[2] = {sizeof(struct sockaddr_in), sizeof(struct sockaddr_in)};
	
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd == -1) {
		perror("socket");
		exit(EXIT_FAILURE);
	}
	
	optval = 1;
	retval = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (void*)&optval, sizeof(optval));
	if(retval == -1) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	
	local.sin_family = AF_INET;
	local.sin_addr.s_addr = inet_addr(addr);
	local.sin_port = htons(port);
	retval = bind(sockfd, (struct sockaddr*)&local, sizeof(struct sockaddr_in));
	if(retval == -1) {
		perror("bind");
		exit(EXIT_FAILURE);
	}
	
	retval = listen(sockfd, 2);
	if(retval == -1) {
		perror("listen");
		exit(EXIT_FAILURE);
	}
	
	struct pollfd entry = {sockfd, POLLIN, 0};
	

	while(1) {
		retval = poll(&entry, 1, -1);
		switch(retval) {
			case -1:
				perror("poll");
				exit(EXIT_FAILURE);
			break;
		
			case 1:
				printf("case 1\n");
				
				newsock[0] = accept(sockfd, (struct sockaddr*)&foreign[0], &foreign_len[0]);
				
				if(newsock[0] == -1) {
					fprintf(stderr, "accept%d\n", 0);
					perror("");
					exit(EXIT_FAILURE);
				}
				
				fprintf(stderr, "Incoming connection from %s:%d\n", inet_ntoa(foreign[0].sin_addr), ntohs(foreign[0].sin_port));

			break;
			
			case 2:
				printf("case 2\n");
				for(i=0; i<2; i++) {
					newsock[i] = accept(sockfd, (struct sockaddr*)&foreign[i], &foreign_len[i]);
					if(newsock[i] == -1) {
						fprintf(stderr, "accept%d\n", i);
						perror("");
						exit(EXIT_FAILURE);
					}
					fprintf(stderr, "Incoming connection from %s:%d\n", inet_ntoa(foreign[i].sin_addr), ntohs(foreign[i].sin_port));

				}
			break;

		}	
	}

	
	
	
}
