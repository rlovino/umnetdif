/*test per gestione flag non bloccanti*/
/*	il flag SOCK_NONBLOCK in socket() oppure il flag MSG_DONTWAIT in recv() non rende le chiamate nonbloccanti,
	questo perché umview evidentemente non gestisce questi flag.
	result) event subscribe viene chiamata ugualmente e la chiamata a recv() resta bloccante*/
	
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <pthread.h>
#include <poll.h>
#include <stdint.h>
#include <arpa/inet.h>

int main(int argc, char *argv[]) {

	char src_addr[16];
	char dst_addr[16];
	uint16_t src_port;
	uint16_t dst_port;
	char buffer[16];

#if !(defined SNBLK || defined MSGDW || defined BOTHFLG)
	fprintf(stderr, "Cannot determine test, please recompile using exactly one of these flag MSGDW, SNBLK, BOTHFLG as argument to gcc, i.e. -DMSGDW\n");
	exit(EXIT_FAILURE);
#endif



	if(argc != 5) {
		fprintf(stderr, "Usage: %s <ip_src_addr> <src_port_no> <ip_dst_addr> <src_dst_no>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	else {
		sprintf(src_addr, "%s", argv[1]);
		src_port = atoi(argv[2]);
		sprintf(dst_addr, "%s", argv[3]);
		dst_port = atoi(argv[4]);
	}
	fprintf(stderr, "SOURCE %s:%d\n", src_addr, src_port);
	fprintf(stderr, "DESTINATION %s:%d\n", dst_addr, dst_port);

	int sockfd;
	int retval;
	int optval;
	struct sockaddr_in local, foreign;

#if (defined SNBLK || defined BOTHFLG)
	fprintf(stderr, "calling socket() with flag SOCK_NONBLOCK\n");
	sockfd = socket(AF_INET, SOCK_STREAM| SOCK_NONBLOCK, 0);
#elif defined MSGDW
	fprintf(stderr, "calling socket() without flag SOCK_NONBLOCK\n");
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
#endif
	if(sockfd == -1) {
		perror("socket");
		exit(EXIT_FAILURE);
	}
	
	optval = 1;
	retval = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (void*)&optval, sizeof(optval));
	if(retval == -1) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	
	local.sin_family = AF_INET;
	local.sin_addr.s_addr = inet_addr(src_addr);
	local.sin_port = htons(src_port);
	retval = bind(sockfd, (struct sockaddr*)&local, sizeof(struct sockaddr_in));
	if(retval == -1) {
		perror("bind");
		exit(EXIT_FAILURE);
	}
	
	foreign.sin_family = AF_INET;
	foreign.sin_addr.s_addr = inet_addr(dst_addr);
	foreign.sin_port = htons(dst_port);
	retval = connect(sockfd, (struct sockaddr*)&foreign, sizeof(struct sockaddr_in));
	if(retval == -1) {
		perror("connect");
		exit(EXIT_FAILURE);
	}
	

#if (defined MSGDW || defined BOTHFLG)
	fprintf(stderr, "calling recv() with flag MSG_DONTWAIT\n");
	retval = recv(sockfd, (void*)&buffer, 16, MSG_DONTWAIT);
#elif defined SNBLK
	fprintf(stderr, "calling recv() without flag MSG_DONTWAIT\n");
	retval = recv(sockfd, (void*)&buffer, 16, 0);
#endif

	
	retval = recv(sockfd, (void*)&buffer, 16, MSG_DONTWAIT);
	if(retval == -1) {
		perror("recv");
		exit(EXIT_FAILURE);
	}

	while(1) sleep(1);
	
	
}
