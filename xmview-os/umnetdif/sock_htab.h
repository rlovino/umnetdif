/*   Copyright 2014 Raffaele Lovino <raff.lovino@gmail.com>
 *   
 *   This is part of umNETDIF
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License, version 2, as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
 *
 */

#ifndef _SOCK_HTAB_H
#define _SOCK_HTAB_H

#include <pthread.h>

/*socket hashtab struct handler*/
struct htab_handler {
	void *htab;
	pthread_mutex_t mutex;
};

/*create hashtab*/
/*return initialized hashtab pointer*/
void *sock_htab_init();

/*destroy hashtab pointed to by 'htab'*/
void sock_htab_destroy(void *htab);

/*add 'key-val'  to hashtab pointed to by 'htab'*/
/*returns 0 in case of success, -1 otherwise (error)*/
int sock_htab_add(void *htab, int key, void *val);

/*remove entry addressed by 'key' from hashtab pointed to by 'htab'*/
/*retval is a value-result parameters which contains address
	of the value to be removed (and to be freed)*/
/*returns 0 in case of success, -1 otherwise (error)*/
int sock_htab_remove(void *htab, int key, void *retval);

/*returns pointer to the value addressed by 'key', NULL in case of error*/
void *sock_htab_get(void *htab, int key);

/*returns size of hashtab pointed to by 'htab'*/
size_t sock_htab_getsize(void *htab);

/*check whether 'key' exists in hashtab pointed to by 'htab'*/
/*returns 1 if 'key' exists, 0 otherwise*/
int sock_htab_containskey(void *htab, int key);




#endif /*_SOCK_HTAB_H*/
