/*   Copyright 2014 Raffaele Lovino <raff.lovino@gmail.com>
 *   
 *   This is part of umNETDIF
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License, version 2, as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <pthread.h>
#include "umnet.h"
#include "umnetdif.h"
#include "umnetdif_utils.h"
#include "umnetdif_types.h"
#include "sock_htab.h"
#include "hsearch_rw/src/hsearch_rw.h"


void *sock_htab_init()
{
	struct htab_handler *handler = (struct htab_handler*)malloc(sizeof(struct htab_handler));
	
	/*init htab mutex*/
	pthread_mutex_init(&handler->mutex, NULL);
	
	handler->htab = (void*)hsearch_rw_new();
	if(handler->htab == NULL) {
		fprintf(stderr, "SOCK_HTAB: sock_htab_init(): Error while creating hashtab handler\n");
		return NULL;
	}
	else
		return (void*)handler;
	
}


void sock_htab_destroy(void *htab)
{
	struct htab_handler *handler = (struct htab_handler*)htab;
	
	/*destroying htab mutex*/
	pthread_mutex_destroy(&handler->mutex);
	hsearch_rw_destroy( (struct hsearch_rw*)handler->htab, NULL, NULL);
	free(handler);
	return;
}


int sock_htab_add(void *htab, int key, void *val)
{
	int rv, rc;
	struct htab_handler *handler = (struct htab_handler*)htab;
	struct hsearch_rw *ptr = (struct hsearch_rw *)(handler->htab);
	char *str_key = strdup(int_to_string(key));
	
	/*lock htab mutex*/
	rc = pthread_mutex_lock(&handler->mutex);
	
	if(!hsearch_rw_containskey(ptr, str_key)) {	
		/*insert item*/
		rv = hsearch_rw_put(ptr, strdup(str_key), val);

		if(rv == 0) {
			fprintf(stderr, "SOCK_HTAB: sock_htab_add(): Error while inserting entry %d in hashtab\n", key);
			/*unlock mutex*/
			rc = pthread_mutex_unlock(&handler->mutex);
			free(str_key);			
			return -1;
		}
		else {
			/*unlock mutex*/
			rc = pthread_mutex_unlock(&handler->mutex);
			free(str_key);		
			return 0;
		}
	}
	else {
		/*unlock mutex*/
		rc = pthread_mutex_unlock(&handler->mutex);
		free(str_key);		
		return -1;
	}

}


int sock_htab_remove(void *htab, int key, void *retval)
{
	int rv, rc;
	struct htab_handler *handler = (struct htab_handler*)htab;
	struct hsearch_rw *ptr = (struct hsearch_rw *)(handler->htab);
	char *str_key = strdup(int_to_string(key));
	
	/*lock htab mutex*/
	rc = pthread_mutex_lock(&handler->mutex);
	
	/*check whether key is contained or not*/
	if(hsearch_rw_containskey(ptr, str_key)) {
		struct hsearch_rw_item *removed = (struct hsearch_rw_item*)malloc(sizeof(struct hsearch_rw_item));
		rv = hsearch_rw_remove(ptr, str_key, removed);
		if(rv == 0) {
			fprintf(stderr, "SOCK_HTAB: sock_htab_remove(): Error while removing entry %d from hashtab\n", key);
			/*unlock mutex*/
			rc = pthread_mutex_unlock(&handler->mutex);
			free(removed);
			free(str_key);
			retval = NULL;				
			return -1;			
		}
		else {
			/*unlock mutex*/
			rc = pthread_mutex_unlock(&handler->mutex);
			free(str_key);			
			retval = removed->val;	//
			return 0;			
		}
	}
	else {
		/*unlock mutex*/
		rc = pthread_mutex_unlock(&handler->mutex);
		retval = NULL;
		free(str_key);
		return -1;
	}
	

}


void *sock_htab_get(void *htab, int key)
{
	int rv, rc;
	struct htab_handler *handler = (struct htab_handler*)htab;
	struct hsearch_rw *ptr = (struct hsearch_rw *)(handler->htab);
	char *str_key = strdup(int_to_string(key));
	void *val = NULL;
	
	/*lock htab mutex*/
	rc = pthread_mutex_lock(&handler->mutex);
	
	/*check whether key is contained or not*/
	if(hsearch_rw_containskey(ptr, str_key)) {
	
		val = hsearch_rw_get(ptr, str_key);
		if(!val) {
			/*unlock mutex*/
			free(str_key);
			return NULL;				
		}
		else {
			/*unlock mutex*/
			rc = pthread_mutex_unlock(&handler->mutex);
			free(str_key);
			return val;		
		}
	

	}
	else {
		/*unlock mutex*/
		rc = pthread_mutex_unlock(&handler->mutex);
		free(str_key);
		return NULL;
	}
	

}

size_t sock_htab_getsize(void *htab)
{
	int rc;
	size_t rv;
	struct htab_handler *handler = (struct htab_handler*)htab;
	struct hsearch_rw *ptr = (struct hsearch_rw *)(handler->htab);	
	
	/*lock htab mutex*/
	rc = pthread_mutex_lock(&handler->mutex);
	
	rv = hsearch_rw_size(ptr);

	/*unlock mutex*/
	rc = pthread_mutex_unlock(&handler->mutex);
	
	return rv;	
	
}


int sock_htab_containskey(void *htab, int key)
{
	int rv, rc;
	struct htab_handler *handler = (struct htab_handler*)htab;
	struct hsearch_rw *ptr = (struct hsearch_rw *)(handler->htab);
	char *str_key = strdup(int_to_string(key));
	
	/*lock htab mutex*/
	rc = pthread_mutex_lock(&handler->mutex);
	
	rv = hsearch_rw_containskey(ptr, str_key);
	
	/*unlock mutex*/
	rc = pthread_mutex_unlock(&handler->mutex);
	
	free(str_key);
	return rv;	
}
