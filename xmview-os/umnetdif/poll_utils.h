/*   Copyright 2014 Raffaele Lovino <raff.lovino@gmail.com>
 *   
 *   This is part of umNETDIF
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License, version 2, as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
 *
 */
 
#ifndef _POLL_UTILS_H
#define _POLL_UTILS_H

/*wrapper for poll() system call*/
/*this function tests the two real_fd at the same time*/
int umnetdif_poll(struct pollfd *fds, nfds_t nfds, int timeout);


/*poll() array management functions*/
/*these functions take array dimension before their calling as input parameter,
and return modified array dimension after their calling*/
/*BE CAREFUL*/
/*ARRAY DIMENSION CAN BE ONLY MODIFIED BY USING THESE FUNCTIONS!
ANY ATTEMPT TO MODIFY DIMENSION MANUALLY LEADS TO UNKNOWN SIDE-EFFECTS*/

/*initialize poll array of size 'n' and returns 0 as initial dimension*/
nfds_t poll_array_init(struct pollfd *fds, int n);

/*add 'entry' to 'fds' set at the end of the array*/
/*'dim' represents the array dimension before the call*/
/*returns increased dimension if 'entry' is successfully inserted, 
  otherwise returns unchanged dimension*/
nfds_t poll_array_add(struct pollfd *fds, struct pollfd entry, nfds_t dim);

/*remove an entry addressed by 'fd' from 'fds' set*/
/*'dim' represents the array dimension before the call*/
/*returns decreased dimension if 'entry' is successfully removed, 
  otherwise returns unchanged dimension*/
nfds_t poll_array_remove(struct pollfd *fds, int fd, nfds_t dim);

/*prints array content from first to dim-th element, only for debug purpose*/
void poll_array_print(struct pollfd *fds, nfds_t dim);


#endif /*_POLL_UTILS_H*/
