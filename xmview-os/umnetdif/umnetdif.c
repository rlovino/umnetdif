/*   Copyright 2014 Raffaele Lovino <raff.lovino@gmail.com>
 *   
 *   This is part of umNETDIF
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License, version 2, as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <sys/types.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <linux/net.h>
#include <linux/sockios.h>
#include <linux/if.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <search.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <poll.h>
#include <sys/time.h>
#include <netinet/tcp.h>
#include <linux/if_tun.h>
#include <stdint.h>

#include "umnet.h"
#include "umnetdif.h"
#include "umnetdif_utils.h"
#include "umnetdif_types.h"
#include "sock_htab.h"
#include "poll_utils.h"
#include "hsearch_rw/src/hsearch_rw.h"




int umnetdif_event_subscribe(void (* cb)(), void *arg, int fd, int how)
{
	int rv;
	int n, retval;
	int maxfd = -1;
	int i;
	struct sock_double_fd *sdfd=NULL;
	struct event_subs_arg params;
	struct pollfd entry;
	
	entry.fd = fd;
	entry.events = how;
	entry.revents = 0;

	dif_debug("entering event_subscribe with cb %x arg %x fd %d how %d\n", cb, arg, fd, how);

	/*check fd status with umnetdif_poll()*/
	rv = umnetdif_poll(&entry, 1, 0);
	dif_debug("poll returned %d\n", rv);
	
	if((rv == 0) || (cb == NULL)) {
		pthread_mutex_lock(&poll_mutex);
		
		/*packaging parameters to send to poll_thread*/
		memset(&params, 0x00, sizeof(params));
		params.cb = cb;
		params.arg = arg;
		params.fd = fd;
		params.how = how;

		/*send parameters to poll_thread*/
		n = write(poll_data_wrpipe, (void*)&params, sizeof(params));
		if(n == -1) {
			dif_perror("write\n");
			pthread_exit(NULL);
		}
		
		pthread_cond_wait(&poll_cv, &poll_mutex);
		pthread_mutex_unlock(&poll_mutex);

		dif_debug("returning %d to umview core\n", rv);
		return rv;	
	}



	switch(rv) {
		case -1:
			dif_debug("returning %d to umview core\n", rv);
			return rv;

		case 1:
			dif_debug("returning %d to umview core\n", entry.revents);
			return entry.revents;
	}

}

void *poll_thread_main()
{
	struct pollfd pipe_set[2], sock_set[MAX_OPEN_FILENO];
	nfds_t sock_set_dim, i, j;
	
	int rv, n, retval = 0;
	struct event_subs_arg event_req;
	
	/*initializing pipe_set, i.e. set of pipe fds for event_subscribe() data exchange*/
	pipe_set[0].fd = poll_command_rdpipe;
	pipe_set[0].events = POLLIN;
	pipe_set[0].revents = 0;
	pipe_set[1].fd = poll_data_rdpipe;
	pipe_set[1].events = POLLIN;
	pipe_set[1].revents = 0;
	
	/*initializing sock_set, i.e. set of socket fds to be monitored*/
	sock_set_dim = poll_array_init(sock_set, MAX_OPEN_FILENO);
	
	while(TRUE) {
		
		/*wait for event_subscribe() 'request for operation' or 'request for termination'*/
		rv = poll(pipe_set, 2, 10);
		
		switch(rv) {
			case -1:
				dif_perror("poll\n");
				pthread_exit(NULL);			
			break;
			
			case 0:

			break;
			
			default:
			{
				if((pipe_set[0].events & pipe_set[0].revents) == POLLIN) {
					char comm;
					n = read(poll_command_rdpipe, &comm, sizeof(comm));
				
					if(comm == 'x')				
						pthread_exit(NULL); /*request for termination*/
				}
			
				if((pipe_set[1].events & pipe_set[1].revents) == POLLIN) {	/*event_subscribe() request for operation*/
				
					pthread_mutex_lock(&poll_mutex);
					memset(&event_req, 0x00, sizeof(event_req));
				
					n = read(poll_data_rdpipe, &event_req, sizeof(struct event_subs_arg));
					if(n == -1) {
						dif_perror("read\n");
						pthread_exit(NULL);
					}
				
					if(event_req.cb == NULL) { /*deregistration*/
						/*	find sockfd-key entry in hashtab*/
						struct sock_double_fd *sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, event_req.fd);
						if(!sdfd) {
							errno = EBADF;
							dif_perror("Unable to get file descriptor %d from hashtable\n", event_req.fd);
						}
						else {
							/*free 'subscription' field from sdfd*/
							free(sdfd->subscription);
							sdfd->subscription = NULL;
							dif_debug("freed subscription for sdfd %d\n", event_req.fd);
							/*remove fd from sock_set*/
							sock_set_dim = poll_array_remove(sock_set, event_req.fd, sock_set_dim);					
						}
						//poll_array_print(sock_set, sock_set_dim);

					}
					else { /*registration*/
						/*	find sockfd-key entry in hashtab*/
						struct sock_double_fd *sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, event_req.fd);
						if(!sdfd) {
							errno = EBADF;
							dif_perror("Unable to get file descriptor %d from hashtable\n", event_req.fd);
						}
						else {
							/*set 'subscription' field in sdfd*/
							sdfd->subscription = (struct event_subs_arg*)malloc(sizeof(struct event_subs_arg));
							memcpy(sdfd->subscription, &event_req, sizeof(struct event_subs_arg));

							struct pollfd entry;

							entry.fd = event_req.fd;
							entry.events = event_req.how;
							entry.revents = 0;
							dif_debug("subscription for fd %d\n", entry.fd);				
							sock_set_dim = poll_array_add(sock_set, entry, sock_set_dim);
						}
						//poll_array_print(sock_set, sock_set_dim);
					}
					pthread_cond_signal(&poll_cv);
					pthread_mutex_unlock(&poll_mutex);					

				}		
			}			
			break;
		}


		/*check for events on monitored fds*/
		retval = umnetdif_poll(sock_set, sock_set_dim, 10);

		int count = retval;
		i=0;
		while( (count > 0) && (i < sock_set_dim)) {
			dif_debug("poll returned %d\n", count);
			/*	find sockfd-key entry in hashtab*/
			struct sock_double_fd *sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sock_set[i].fd);
			if(!sdfd) {
				errno = EBADF;
				dif_perror("event check: Unable to get file descriptor %d from hashtable\n", sock_set[i].fd);
			}
			else {
				struct event_subs_arg *ptr = sdfd->subscription;
				if(!ptr) {
					dif_debug("subscription field for fd %d is NULL\n", sock_set[i].fd);
				}
				else {
					if((sock_set[i].revents & ptr->how) == ptr->how) { /* if 'how' and 'revents' fields match, event happened*/
						count--;
						dif_debug("executing callback %x arg %x\n", ptr->cb, ptr->arg);
						
						ptr->cb(ptr->arg); /*execute callback function*/			
						
						dif_debug("cb %x executed\n", ptr->cb);

					}				
				}
			}
			i++;			
		}
	}
}


int umnetdif_supported_domain(int domain)
{
	switch(domain) {
		case AF_INET:
			return 1;
		default:
			return 0;
	}
}


int umnetdif_msocket(int domain, int type, int protocol, struct umnet *nethandle) {

	struct sock_double_fd *sdfd=NULL;
	int generic_fd;
	int rc, i;

	dif_debug("domain %d\ttype %d\tprotocol %d\n", domain, type, protocol);

	switch(domain) {
		case AF_INET:
			sdfd = (struct sock_double_fd*)malloc(sizeof(struct sock_double_fd));
			memset(sdfd, 0x00, sizeof(struct sock_double_fd));

			/*	generic_fd: this will be returned to umnet module.
				This fd represents socket hashtab entry key and it will not
				be used for I/O*/
			generic_fd = open("/dev/null", O_RDONLY);
			if(generic_fd == -1) {
				dif_perror("error on open() = %d ", generic_fd);
				return -1;
			}
			dif_debug("created generic_fd = %d\n", generic_fd);

			/*	creation of real sockets associated with generic_fd*/	
			for(i=0; i<2; i++) {
				sdfd->sfd[i] = socket(domain, type, protocol);
				if(sdfd->sfd[i] == -1) 
					dif_perror("real_fd[%d] ", i);
				else
					dif_debug("created real_fd[%d] = %d\n", i, sdfd->sfd[i]);

				/*set usage flag*/
				SETUSGFLG(sdfd->flags, USE_FIRST << i);
				
			}
			
			/*set type flag*/
			int stype = type & (SOCK_DGRAM|SOCK_STREAM);
			switch(stype) {
				case SOCK_DGRAM:	SETTYPEFLG(sdfd->flags, SOCKTYPE_DGRAM);
				break;
				
				case SOCK_STREAM:	SETTYPEFLG(sdfd->flags, SOCKTYPE_STREAM);
				break;
			}
			dif_debug("generic_fd %d flags 0x%x\n", generic_fd, sdfd->flags);
			

			rc = sock_htab_add(file_htab, generic_fd, (void *)sdfd);
			if(rc == -1) {
				dif_debug("ERROR: Unable to add file descriptor %d to hash table\n", generic_fd);
				free(sdfd);
				return -1;
			}
			dif_debug("socket %d added to socket hashtab\n", generic_fd);
			return generic_fd;
			
		default:
			errno = EAFNOSUPPORT;
			dif_perror("domain = %d\n", domain);
			return -1; 
	}
}


int umnetdif_setsockopt(int sockfd, int level, int optname, const void *optval, socklen_t optlen)
{
	struct sock_double_fd *sdfd=NULL;
	int rv[2], i;
	
	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	dif_debug("sockfd %d level %d optname %d optval 0x%x optlen %d\n",
				sockfd, level, optname, optval, optlen);
	
	
	if(GETUSGFLG(sdfd->flags) == USE_FIRST) {
		dif_debug("socket %d has usage flag set to USE_FIRST\n", sockfd);
		/*sdfd is marked as USE_FIRST, so only first real fd will be used, second is discarded*/
		/*call to host system setsockopt()*/
		rv[0] = setsockopt(sdfd->sfd[0], level, optname, optval, optlen);
		if(rv[0] == -1) {
			dif_perror("real_fd[%d] ", 0);
		}
		
		return rv[0];
	}
	if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
		dif_debug("socket %d has usage flag set to USE_SECOND\n", sockfd);
		/*sdfd is marked as USE_SECOND, so only second real fd will be used, first is discarded*/
		/*call to host system setsockopt()*/
		rv[1] = setsockopt(sdfd->sfd[1], level, optname, optval, optlen);
		if(rv[1] == -1) {
			dif_perror("real_fd[%d] ", 1);
		}

		return rv[1];
	}
	if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
		dif_debug("socket %d has usage flag set to USE_BOTH\n", sockfd);
		/*sdfd is marked as USE_BOTH, so both real fds will be used*/
		for(i=0; i<2; i++) {
			/*call to host system setsockopt()*/
			rv[i] = setsockopt(sdfd->sfd[i], level, optname, optval, optlen);
			if(rv[i] == -1) {
				dif_perror("real_fd[%d] ", i);
			}
		}
	
		if( (rv[0] == -1) || (rv[1] == -1) ){ /*if one setsockopt() call fails, error is returned*/
			return -1;
		}
		else {
			dif_debug("exit with success\n");
			return 0; /*both setsockopt() calls terminated with success, return success*/
		}
	}
	
		
}

int umnetdif_getsockopt(int sockfd, int level, int optname, void *optval, socklen_t *optlen) {

	struct sock_double_fd *sdfd=NULL;
	int rv[2], i;
	
	/*	'optlen' is a value-result argument, so its content must be,
		at the end of umnetdif_getsockopt(), the same for the two sockets.
		Its initial content will be stored in 'temp_optlen'.
		At the end, if the two values (temp_optlen[0] and temp_optlen[1]) are different, an error occurred.
		This implementation of getsockopt() will succesfully terminate only if the two values are equal*/
	socklen_t temp_optlen[2];
	void *temp_optval[2];
	

	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	dif_debug("sockfd %d level %d optname %d optval 0x%x optlen %d\n",
				sockfd, level, optname, optval, optlen);
	
	if(GETUSGFLG(sdfd->flags) == USE_FIRST) {
		dif_debug("socket %d has usage flag set to USE_FIRST\n", sockfd);
		/*sdfd is marked as USE_FIRST, so only first real fd will be used, second is discarded*/
		/*call to host system getsockopt()*/
		rv[0] = getsockopt(sdfd->sfd[0], level, optname, optval, optlen);
		if(rv[0] == -1) {
			dif_perror("real_fd[%d] ", 0);
		}


		return rv[0];		
	}
	if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
		dif_debug("socket %d has usage flag set to USE_SECOND\n", sockfd);
		/*sdfd is marked as USE_SECOND, so only second real fd will be used, first is discarded*/
		/*call to host system getsockopt()*/
		rv[1] = getsockopt(sdfd->sfd[1], level, optname, optval, optlen);
		if(rv[1] == -1) {
			dif_perror("real_fd[%d] ", 1);
		}


		return rv[1];		
	}	
	if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
		dif_debug("socket %d has usage flag set to USE_BOTH\n", sockfd);
		/*sdfd is marked as USE_BOTH, so both real fds will be used*/
		
		temp_optlen[0] = temp_optlen[1] = *optlen;
		temp_optval[0] = malloc(sizeof(socklen_t));
		temp_optval[1] = malloc(sizeof(socklen_t));
		
		for(i=0; i<2; i++) {	
			/*call to host system getsockopt()*/		
			rv[i] = getsockopt(sdfd->sfd[i], level, optname, temp_optval[i], &temp_optlen[i]);
			if(rv[i] == -1) {
				dif_perror("real_fd[%d] ", i);
			}
		}	

		if(	((rv[0] == -1) || (rv[1] == -1))	||  /*error occurred in one or two calls*/
			(temp_optlen[0] != temp_optlen[1])	||  /*error: the two 'optlen' are different*/
			(memcmp(temp_optval[0], temp_optval[1], *optlen) != 0) ) { /*error: the two 'optval' buffers are different*/

			free(temp_optval[0]);
			free(temp_optval[1]);

			return -1;
		}
		else { /*everything is ok, so 'temp_optval' buffer is copied in the caller's buffer, and caller's 'optlen' is updated*/

			memcpy(optval, temp_optval[0], temp_optlen[0]); /*no difference between first or last array value*/
			*optlen = temp_optlen[0];
			dif_debug("exit with success\n");
			return 0;
		}	
	}
}



int umnetdif_bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{

	struct sock_double_fd *sdfd=NULL;
	int rv[2], i;

	struct sockaddr_in local[2];
	
	uint32_t choosen_addr;
	uint16_t double_port, choosen_port;
	int ret;


	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	dif_debug("sockfd %d addr %s:%d addrlen %d\n",
				sockfd, inet_ntoa(((struct sockaddr_in*)addr)->sin_addr),
				ntohs(((struct sockaddr_in*)addr)->sin_port),
				addrlen);

	uint32_t origin_addr = ((struct sockaddr_in*)addr)->sin_addr.s_addr;
	uint16_t origin_port = ((struct sockaddr_in*)addr)->sin_port;

	if(	(origin_addr == virt_if_ipv4_num_addr) ||
		(origin_addr == INADDR_ANY)) {
		/*if socket is bound to INADDR_ANY or to VIRTIF_ADDR (10.0.6.66)*/
		dif_debug("socket %d is bound to INADDR_ANY or to VIRTIF_ADDR\n", sockfd);
		
		if(ntohs(origin_port) == 0) {
			dif_debug("port choosen by process %d\n", 0);
			/* case anyport (0): we must assign a random port number, but it should be the same for both sockets*/
			int socktype;
			if(GETTYPEFLG(sdfd->flags) == SOCKTYPE_DGRAM)
				socktype = SOCK_DGRAM;
			else if(GETTYPEFLG(sdfd->flags) == SOCKTYPE_STREAM)
				socktype = SOCK_STREAM;
			do {
				double_port = get_rand_port();
				dif_debug("check if random port %d is free\n", htons(double_port));				
				ret = port_is_free(INADDR_ANY, double_port, socktype);
				/*if 'double_port' is not free to use, choose another one*/
			}
			while(ret < 1);
			choosen_port = htons(double_port);
			dif_debug("port choosen by umnetdif %d\n", htons(double_port));
		}
		else { 
			/*in this case, port number has been choosen by caller, so I will use that*/
			choosen_port = ntohs(origin_port);
			dif_debug("port choosen by process %hu\n", choosen_port);
		}

		for(i=0; i<2; i++) {		
			local[i].sin_family = AF_INET;
			inet_pton(AF_INET, interface[i].ipv4addr, &local[i].sin_addr);
			local[i].sin_port = htons(choosen_port);

			/*call to host system bind()*/
			rv[i] = bind(sdfd->sfd[i], (struct sockaddr*)&local[i], sizeof(struct sockaddr_in));
			if(rv[i] == -1) {
				dif_perror("real_fd[%d] ", i);
			}

		}
		
		if((rv[0] == -1) || (rv[1] == -1)) {
			/*returns an error even if errors occurred on one socket*/
			dif_debug("error rv[0] %d rv[1] %d\n", rv[0], rv[1]);
			return -1;
		}
		
		/*set flag to ANY_BOUND*/
		SETBNDFLG(sdfd->flags, ANY_BOUND);

		/*for each real fd, store local addr info*/
		for(i=0; i<2; i++) {
			sdfd->local[i] = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
			memset(sdfd->local[i], 0x00, sizeof(struct sockaddr_in));
			memcpy((void*)sdfd->local[i], (void*)&local[i], sizeof(local[i]));

		}

		/*allocate generic_addr struct*/
		sdfd->generic_local = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
		memset(sdfd->generic_local, 0x00, sizeof(struct sockaddr_in));

		/*copy struct sockaddr_in bind address: this will be the address returned by a call to getsockname()*/
		memcpy( (void*)sdfd->generic_local, (void*)addr, sizeof(struct sockaddr_in));

		/*store port number*/
		((struct sockaddr_in*)sdfd->generic_local)->sin_port = htons(choosen_port);
		
		dif_debug("exit with success\n");
		return 0;		
			
	} else if(	(origin_addr == htonl(INADDR_LOOPBACK))) {
		/*socket is bound to 127.0.0.1*/
		dif_debug("socket %d is bound to INADDR_LOOPBACK\n", sockfd);
		
		/*close second (unuseful) socket*/
		close(sdfd->sfd[1]);
		sdfd->sfd[1] = -1;

		if(ntohs(origin_port) == 0) {
			dif_debug("port choosen by process %d\n", 0);
			/* case anyport (0): we must assign a random port number, but it should be the same for both sockets*/
			int socktype;
			if(GETTYPEFLG(sdfd->flags) == SOCKTYPE_DGRAM)
				socktype = SOCK_DGRAM;
			else if(GETTYPEFLG(sdfd->flags) == SOCKTYPE_STREAM)
				socktype = SOCK_STREAM;
			do {
				double_port = get_rand_port();
				dif_debug("check if random port %d is free\n", htons(double_port));				
				ret = port_is_free(INADDR_LOOPBACK, double_port, socktype);
				/*if 'double_port' is not free to use, choose another one*/
			}
			while(ret < 1);
			choosen_port = htons(double_port);
			dif_debug("port choosen by umnetdif %d\n", htons(double_port));
		}
		else {
			/*in this case, port number has been choosen by caller, so I will use that*/
			choosen_port = ntohs(origin_port);
			dif_debug("port choosen by process %d\n", choosen_port);
		}		

		local[0].sin_family = AF_INET;
		inet_pton(AF_INET, "127.0.0.1", &local[0].sin_addr);
		local[0].sin_port = htons(choosen_port);

		/*call to host system bind()*/
		rv[0] = bind(sdfd->sfd[0], (struct sockaddr*)&local[0], sizeof(struct sockaddr_in));
		if(rv[0] == -1) {
			dif_perror("real_fd[%d] ", 0);
			return -1;
		}

		/*set LOCAL_BOUND flag*/
		SETBNDFLG(sdfd->flags, LOCAL_BOUND);
		/*reset USE_SECOND flag (previously set by msocket()*/
		RESETUSGFLG(sdfd->flags, USE_SECOND);
		
		/*save local addr info*/
		sdfd->local[0] = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
		memset(sdfd->local[0], 0x00, sizeof(struct sockaddr_in));
		memcpy((void*)sdfd->local[0], (void*)&local[0], sizeof(struct sockaddr_in));

		/*allocate generic_addr struct*/
		sdfd->generic_local = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
		memset(sdfd->generic_local, 0x00, sizeof(struct sockaddr_in));

		/*copy struct sockaddr_in bind address: this will be the address returned by a call to getsockname()*/
		memcpy( (void*)sdfd->generic_local, (void*)addr, sizeof(struct sockaddr_in));
		/* store port number */
		((struct sockaddr_in*)sdfd->generic_local)->sin_port = htons(choosen_port);

		dif_debug("exit with success\n");
		return 0;		

	}
	else {
		/*error, cannot bind*/
		errno = EADDRNOTAVAIL;
		dif_perror("origin_addr = %d", origin_addr);
		return -1;	
	}
}


int umnetdif_shutdown(int sockfd, int how)
{
	struct sock_double_fd *sdfd=NULL;
	int rv[2], rc, i, index;
	int n;
	
	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	dif_debug("sockfd %d how %d\n", sockfd, how);
	
	if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
		dif_debug("sockfd %d usage flag is set to USE_BOTH\n", sockfd);
		for(i=0; i<2; i++) 
			rv[i] = shutdown(sdfd->sfd[i], how);
		return max(rv[0], rv[1]);
	}
	
	if(GETUSGFLG(sdfd->flags) == USE_FIRST) {
		dif_debug("sockfd %d usage flag is set to USE_FIRST\n", sockfd);
		index = 0;
	}
	if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
		dif_debug("sockfd %d usage flag is set to USE_SECOND\n", sockfd);
		index = 1;
	}

	return shutdown(sdfd->sfd[index], how);
	
}


int umnetdif_close(int sockfd)
{
	struct sock_double_fd *sdfd=NULL;
	int rv[2], rc, i, index;
	int n;
	
	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	dif_debug("sockfd %d\n", sockfd);

	if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
		dif_debug("sockfd %d usage flag is set to USE_BOTH\n", sockfd);
		/*sdfd is marked as USE_BOTH, so both real fds will be closed*/
		/*close real_fd*/
		for(i=0; i<2; i++) { 
			rv[i] = close(sdfd->sfd[i]);
		}
		
		/*close generic_fd*/
		rv[0] = close(sockfd);
		
		/*remove entry from file hashtab*/
		struct sock_double_fd *rem;
		rv[0] = sock_htab_remove(file_htab, sockfd, (void*)rem);
		if(rv[0] == -1) {
			errno = EBADF;
			dif_perror("Unable to remove file descriptor %d from poll hashtable ", sockfd);
			return -1;
		}						
		else {
			/*free all structs pointed to by sdfd*/
			for(i=0; i<2; i++) {
				if(sdfd->local[i])
					free(sdfd->local[i]);
				if(sdfd->foreign[i]) 
					free(sdfd->foreign[i]);					
			}
			
			if(sdfd->generic_local) 
				free(sdfd->generic_local);
						
			if(sdfd->generic_foreign) 
				free(sdfd->generic_foreign);	
			
			if(sdfd->subscription) { 
				;/*TODO*/
			}	
			
			free(sdfd);
			free(rem);
			dif_debug("exit with success\n");
			return 0; 
		}			
	}


	if(GETUSGFLG(sdfd->flags) == USE_FIRST){
		dif_debug("sockfd %d usage flag is set to USE_FIRST\n", sockfd);
		index = 0;
	}
	if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
		dif_debug("sockfd %d usage flag is set to USE_SECOND\n", sockfd);
		index = 1;
	}
	/*sdfd is marked as USE_{FIRST|SECOND}, so only one real fd will be closed*/
	/*close real_fd*/
	rv[index] = close(sdfd->sfd[index]);

	/*close generic_fd*/
	rv[index] = close(sockfd);

	/*remove entry from file hashtab*/
	struct sock_double_fd *rem;
	rv[0] = sock_htab_remove(file_htab, sockfd, (void*)rem);
	if(rv[0] == -1) {
		errno = EBADF;
		dif_perror("Unable to remove file descriptor %d from poll hashtable ", sockfd);
		return -1;
	}			
	else {
		/*free all structs pointed to by sdfd*/
		if(sdfd->local[index])
			free(sdfd->local[index]);
		
		if(sdfd->foreign[index]) 
			free(sdfd->foreign[index]);					
		

		if(sdfd->generic_local) 
			free(sdfd->generic_local);
						
		if(sdfd->generic_foreign) 
			free(sdfd->generic_foreign);	
			
		if(sdfd->subscription) { 
			; /*TODO*/
		}			
				
		free(sdfd);
		free(rem);
	
		dif_debug("exit with success\n");
		return 0; 
	}
}


int umnetdif_getpeername(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
	struct sock_double_fd *sdfd=NULL;
	int rc;
	
	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}	
	dif_debug("sockfd %d\n", sockfd);
	
	if(sdfd->generic_foreign) {
		memcpy((void*)addr, (void*)sdfd->generic_foreign, *addrlen);
		*addrlen = sizeof(struct sockaddr_in);
		
		dif_debug("peer address %s:%d\n",
			inet_ntoa(((struct sockaddr_in*)addr)->sin_addr),
			ntohs(((struct sockaddr_in*)addr)->sin_port));
		dif_debug("exit with success\n");
		return 0;
	}
	else {
		errno = ENOTCONN;
		dif_perror("Socket fd %d is not connected ", sockfd);
		return -1;	
	}
}



int umnetdif_getsockname(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
	/*	due to the inability to retrieve original informations about caller' bind address,
		this function returns address stored in struct sock_double_fd, previously filled by umnetdif_bind()*/
	struct sock_double_fd *sdfd=NULL;
	int rc;
	
	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}	
	dif_debug("sockfd %d\n", sockfd);
	
	if(sdfd->generic_local) {
		memcpy( (void*)addr, (void*)sdfd->generic_local, *addrlen);
		*addrlen = sizeof(struct sockaddr_in);

		dif_debug("local address %s:%d\n",
			inet_ntoa(((struct sockaddr_in*)addr)->sin_addr),
			ntohs(((struct sockaddr_in*)addr)->sin_port));
		dif_debug("exit with success\n");
		return 0;
	}
	else {
		errno = EINVAL;
		dif_perror("Socket %d is not bound ", sockfd);
		return -1;		
	}
}



int umnetdif_init(char *source, char *mountpoint, unsigned long flags, char *args, struct umnet *nethandle) {

	int rc;
	int i;
	int pipe1[2];
	int pipe2[2];

	char *argv[2] = {NULL, NULL};

	argv[0] = strtok(args, ",");
	argv[1] = strtok(NULL, ",");
	
		
	//get virtual interface IPv4 address (there is no virtual interface for now)
	rc = get_virt_if_ipv4_addr(&virt_if_ipv4_addr);
	if(rc == -1) {
		fprintf(stderr, "Unable to get virtual interface IPv4 address...\n");
		pthread_exit(NULL);
	}
	else{
		fprintf(stderr, "VIRTIF_ADDR 0x%x %s\n", virt_if_ipv4_addr.s_addr, inet_ntoa(virt_if_ipv4_addr));
		virt_if_ipv4_num_addr = virt_if_ipv4_addr.s_addr;
	}

	//interface status table setup
	for(i=0; i<2; i++) {
		memset(&interface[i], 0x00, sizeof(struct ifstatus));
		sprintf(interface[i].name, argv[i]);
		get_interface_ipv4_addr(interface[i].name, interface[i].ipv4addr);
		fprintf(stderr, "interface[%d] = %s (%s)\n", i, interface[i].name, interface[i].ipv4addr);		
	
	}

	// file descriptor hash table setup
	fprintf(stderr, "creating file hash table...\n");
	file_htab = sock_htab_init();
	if(!file_htab) {
		fprintf(stderr, "Unable to create file hash tab...killing process\n");
		pthread_exit(NULL);
	}


	//initializing pipe for poll_thread synchronization
	rc = pipe(pipe1);
	if(rc == -1) {
		perror("pipe");
		pthread_exit(NULL);
	}
	rc = pipe(pipe2);
	if(rc == -1) {
		perror("pipe");
		pthread_exit(NULL);
	}
		
	poll_command_wrpipe = pipe1[1]; /*event_subscribe()-side of command pipe, write-end*/
	poll_command_rdpipe = pipe1[0]; /*poll_thread-side of command pipe, read-end*/
	
	poll_data_wrpipe = pipe2[1];	/*event_subscribe()-side of data pipe, write-end*/
	poll_data_rdpipe = pipe2[0];	/*poll_thread-side of data pipe, read-end*/


	pthread_mutex_init(&poll_mutex, NULL);
	pthread_cond_init(&poll_cv, NULL);
	
	pthread_attr_init(&poll_thread_attr);
	pthread_attr_setdetachstate(&poll_thread_attr, PTHREAD_CREATE_JOINABLE);
	fprintf(stderr, "launching poll thread...\n");
	rc = pthread_create(&poll_thread, NULL, poll_thread_main, NULL);
	if(rc) {
		perror("pthread_create");
		pthread_exit(NULL);
	}
	
	if(DEBUG) fprintf(stderr, "DEBUG mode enabled\n");

	return 0;
}

int umnetdif_fini(struct umnet *nethandle) {

	int rc=0;
	
	fprintf(stderr, "umnetdif submodule fini\n");
	fprintf(stderr, "killing poll thread...\n");
	/*send 'request for termination' command to poll_thread*/
	write(poll_command_wrpipe, "x", 1);
	rc = pthread_join(poll_thread, NULL);
	if(rc) {
		perror("pthread_join");
	}	
		
	pthread_mutex_destroy(&poll_mutex);
	pthread_cond_destroy(&poll_cv);
		
	fprintf(stderr, "destroying file hash table...\n");
	sock_htab_destroy(file_htab);

	pthread_exit(NULL);
	return 0;
}

ssize_t umnetdif_write(int fd, const void *buf, size_t count)
{
	struct sock_double_fd *sdfd=NULL;
	int rv[2], retval, i, index;

	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, fd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", fd);
		return -1;
	}		
	dif_debug("fd %d buf_addr 0x%x count %d\n", fd, buf, count);

	fd_set write_fds;
	int maxfd = -1;
	struct timeval tv;
	
	
	while(TRUE) {	
	
		if(GETBNDFLG(sdfd->flags) == LOCAL_BOUND) {
			dif_debug("sockfd %d bind flag is set to LOCAL_BOUND\n", fd);
			/*sdfd is bound to 127.0.0.1*/
			maxfd = -1;
			FD_ZERO(&write_fds);
			FD_SET(sdfd->sfd[0], &write_fds);
			
			if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
		}
		else if(GETBNDFLG(sdfd->flags) == ANY_BOUND) {
			dif_debug("sockfd %d bind flag is set to ANY_BOUND\n", fd);
			/*sdfd is bound to INADDR_ANY or VIRTIF_ADDR (10.0.6.66)*/
			if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
				dif_debug("sockfd %d usage flag is set to USE_BOTH\n", fd);
				/*sdfd is marked as USE_BOTH, so both real fds will be used to perform write()*/
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[0], &write_fds);
				FD_SET(sdfd->sfd[1], &write_fds);
	
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];			
			}		

			if(GETUSGFLG(sdfd->flags) == USE_FIRST) {
				dif_debug("sockfd %d usage flag is set to USE_FIRST\n", fd);
				/*sdfd is marked as USE_FIRST, so only first real fd will be used to perform write()*/
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[0], &write_fds);
			
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
			}
			if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
				dif_debug("sockfd %d usage flag is set to USE_SECOND\n", fd);
				/*sdfd is marked as USE_SECOND, so only second real fd will be used to perform write()*/
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[1], &write_fds);
		
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];
			}
		}	

		tv.tv_sec = 1;
		tv.tv_usec = 0;

		retval = select(maxfd+1, NULL, &write_fds, NULL, &tv);
		
		switch(retval) {
			case 2:
				dif_debug("both real_fd are ready, case %d\n", 2);
				/*both real_fds are ready to perform write()*/
				for(i=0; i<2; i++) {
					/*call to host system write()*/
					rv[i] = write(sdfd->sfd[i], buf, count);
					if(rv[i] == -1) {
						dif_perror("real_fd[%d] ", i);
					}
				}

				/*TODO: this policy could be changed*/
				if( (rv[0] == -1) && (rv[1] == -1) )	return -1; 	/*error on the two writes*/
				if( (rv[0] == -1) && (rv[1] >= 0) ) 	return rv[1];	/*error on first write, success on second*/
				if( (rv[0] >= 0)  && (rv[1] == -1) )	return rv[0];	/*success on first write, error on second*/
				if( (rv[0] >= 0)  && (rv[1] >= 0) )	return rv[0];	/*both success*/
				
			case 1:

				/*only one real fd is ready to perform write()*/
				for(i=0; i<2; i++) {
					if(FD_ISSET(sdfd->sfd[i], &write_fds)) {
						index = i;
						break; /* choose the available socket*/
					}
				}
				dif_debug("only real_fd[%d] is ready, case %d\n", index, retval);	
				/*call to host system write()*/
				rv[index] = write(sdfd->sfd[index], buf, count);
				if(rv[index] == -1) {
					dif_perror("real_fd[%d] ", index);
				}
				return rv[index];			
		
			case -1:
				dif_perror("error on select() = %d ", retval);
				return -1;				
		
			case 0:
				return 0;
		}
		
	}

}

ssize_t umnetdif_send(int sockfd, const void *buf, size_t len, int flags)
{

	struct sock_double_fd *sdfd=NULL;
	int rv[2], retval, i, index;

	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}		
	dif_debug("sockfd %d buf_addr 0x%x len %d flags 0x%x\n", sockfd, buf, len, flags);

	fd_set write_fds;
	int maxfd = -1;
	struct timeval tv;
	
	
	while(TRUE) {	

		if(GETBNDFLG(sdfd->flags) == LOCAL_BOUND) {	
			dif_debug("sockfd %d bind flag is set to LOCAL_BOUND\n", sockfd);
			/*sdfd is bound to 127.0.0.1*/
			maxfd = -1;
			FD_ZERO(&write_fds);
			FD_SET(sdfd->sfd[0], &write_fds);
			
			if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
		}
		else if(GETBNDFLG(sdfd->flags) == ANY_BOUND) {
			dif_debug("sockfd %d bind flag is set to ANY_BOUND\n", sockfd);
			/*sdfd is bound to INADDR_ANY or VIRTIF_ADDR (10.0.6.66)*/
			if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
				dif_debug("sockfd %d usage flag is set to USE_BOTH\n", sockfd);
				/*sdfd is marked as USE_BOTH, so both real fds will be used to perform send()*/
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[0], &write_fds);
				FD_SET(sdfd->sfd[1], &write_fds);
	
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];			
			}		

			if(GETUSGFLG(sdfd->flags) == USE_FIRST) {
				dif_debug("sockfd %d usage flag is set to USE_FIRST\n", sockfd);
				/*sdfd is marked as USE_FIRST, so only first real fd will be used to perform send()*/
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[0], &write_fds);
			
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
			}
			if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
				dif_debug("sockfd %d usage flag is set to USE_SECOND\n", sockfd);
				/*sdfd is marked as USE_SECOND, so only second real fd will be used to perform send()*/
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[1], &write_fds);
		
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];
			}
		}	
	
		tv.tv_sec = 1;
		tv.tv_usec = 0;

		retval = select(maxfd+1, NULL, &write_fds, NULL, &tv);
		
		switch(retval) {
			case 2:
				/*both real_fds are ready to perform send()*/
				for(i=0; i<2; i++) {
					/*call to host system send()*/
					rv[i] = send(sdfd->sfd[i], buf, len, flags);
					if(rv[i] == -1) {
						dif_perror("real_fd[%d] ", i);
					}
				}

				if( (rv[0] == -1) && (rv[1] == -1) )	return -1; 	/*error on the two send*/
				if( (rv[0] == -1) && (rv[1] >= 0) ) 	return rv[1];	/*error on first send, success on second*/
				if( (rv[0] >= 0)  && (rv[1] == -1) )	return rv[0];	/*success on first send, error on second*/
				if( (rv[0] >= 0)  && (rv[1] >= 0) )	return rv[0];	/*both success*/

			
			case 1:
				/*only one real fd is ready to perform send()*/
				for(i=0; i<2; i++) {
					if(FD_ISSET(sdfd->sfd[i], &write_fds)) {
						index = i;
						break; /* choose the available socket*/
					}
				}	
				/*call to host system send()*/
				rv[index] = send(sdfd->sfd[index], buf, len, flags);
				if(rv[index] == -1) {
					dif_perror("real_fd[%d] ", index);
				}
				return rv[index];			
			
			case -1:
				dif_perror("error on select() = %d ", retval);
				return -1;				
			
			case 0:
				return 0;
		}
		
	}
}





ssize_t umnetdif_sendto(int sockfd, const void *buf, size_t len, int flags,
			const struct sockaddr *dest_addr, socklen_t addrlen)
{
	struct sock_double_fd *sdfd=NULL;
	int rv[2], retval, i, index;

	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	dif_debug("sockfd %d buf_addr %d len %d flags 0x%x\n", sockfd, buf, len, flags);
	
	fd_set write_fds;
	int maxfd = -1;
	struct timeval tv;
	
	if((dest_addr == NULL) && (addrlen == 0)) {
		/*redirecting call to send()*/
		return umnetdif_send(sockfd, buf, len, flags);
	}
	dif_debug("dest_addr %s:%d addrlen %d\n",
			inet_ntoa(((struct sockaddr_in*)dest_addr)->sin_addr),
			ntohs(((struct sockaddr_in*)dest_addr)->sin_port));
	
	
	while(TRUE) {	

		if(GETBNDFLG(sdfd->flags) == LOCAL_BOUND) {
			dif_debug("sockfd %d bind flag is set to LOCAL_BOUND\n", sockfd);
			/*sdfd is bound to 127.0.0.1*/
			maxfd = -1;
			FD_ZERO(&write_fds);
			FD_SET(sdfd->sfd[0], &write_fds);
			
			if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
		}
		else if(GETBNDFLG(sdfd->flags) == ANY_BOUND) {
			dif_debug("sockfd %d bind flag is set to ANY_BOUND\n", sockfd);
			/*sdfd is bound to INADDR_ANY or VIRTIF_ADDR (10.0.6.66)*/
			if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
				dif_debug("sockfd %d usage flag is set to USE_BOTH\n", sockfd);
				/*sdfd is marked as USE_BOTH, so both real fds will be used to perform sendto()*/
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[0], &write_fds);
				FD_SET(sdfd->sfd[1], &write_fds);
	
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];			
			}		

			if(GETUSGFLG(sdfd->flags) == USE_FIRST) {
				dif_debug("sockfd %d usage flag is set to USE_FIRST\n", sockfd);
				/*sdfd is marked as USE_FIRST, so only first real fd will be used to perform sendto()*/
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[0], &write_fds);
			
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
			}

			if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
				dif_debug("sockfd %d usage flag is set to USE_SECOND\n", sockfd);
				/*sdfd is marked as USE_SECOND, so only second real fd will be used to perform sendto()*/
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[1], &write_fds);
		
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];
			}
		}	
	
		tv.tv_sec = 1;
		tv.tv_usec = 0;

		retval = select(maxfd+1, NULL, &write_fds, NULL, &tv);
	
		switch(retval) {
			case 2:
				/*both real_fds are ready to perform sendto()*/
				for(i=0; i<2; i++) {
					/*call to host system sendto()*/
					rv[i] = sendto(sdfd->sfd[i], buf, len, flags, dest_addr, addrlen);
					if(rv[i] == -1) {
						dif_perror("real_fd[%d] ", i);
					}
				}

				if( (rv[0] == -1) && (rv[1] == -1) )	return -1; 	/*error on the two sendto*/
				if( (rv[0] == -1) && (rv[1] >= 0) ) 	return rv[1];	/*error on first sendto, success on second*/
				if( (rv[0] >= 0)  && (rv[1] == -1) )	return rv[0];	/*success on first sendto, error on second*/
				if( (rv[0] >= 0)  && (rv[1] >= 0) )		return rv[0];	/*both success*/

			case 1:
				/*only one real fd is ready to perform sendto()*/
				for(i=0; i<2; i++) {
					if(FD_ISSET(sdfd->sfd[i], &write_fds)) {
						index = i;
						break; /* choose the available socket*/
					}
				}	
				/*call to host system sendto()*/
				rv[index] = sendto(sdfd->sfd[index], buf, len, flags, dest_addr, addrlen);
				if(rv[index] == -1) {
					dif_perror("real_fd[%d] ", index);
				}
				return rv[index];			
			
			case -1:
				dif_perror("error on select() = %d ", retval);
				return -1;				
			
			case 0:
				return 0;
		}
		
	}
}



ssize_t umnetdif_sendmsg(int sockfd, const struct msghdr *msg, int flags)
{
	struct sock_double_fd *sdfd=NULL;
	int rv[2], retval, i, index;

	/*	find sockfd-key entry in hashtab*/	
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable\n", sockfd);
		return -1;
	}	

	fd_set write_fds;
	int maxfd = -1;
	struct timeval tv;

	
	while(TRUE) {	

		if(GETBNDFLG(sdfd->flags) == LOCAL_BOUND) {	
			/*sdfd is bound to 127.0.0.1*/		
			maxfd = -1;
			FD_ZERO(&write_fds);
			FD_SET(sdfd->sfd[0], &write_fds);
			
			if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
		}
		else if(GETBNDFLG(sdfd->flags) == ANY_BOUND) {
			/*sdfd is bound to INADDR_ANY or VIRTIF_ADDR (10.0.6.66)*/		
			if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
				/*sdfd is marked as USE_BOTH, so both real fds will be used to perform sendmsg()*/			
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[0], &write_fds);
				FD_SET(sdfd->sfd[1], &write_fds);
	
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];			
			}		

			if(GETUSGFLG(sdfd->flags) == USE_FIRST) {
				/*sdfd is marked as USE_FIRST, so only first real fd will be used to perform sendmsg()*/			
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[0], &write_fds);
			
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
			}

			if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
				/*sdfd is marked as USE_SECOND, so only second real fd will be used to perform sendmsg()*/			
				maxfd = -1;
				FD_ZERO(&write_fds);
				FD_SET(sdfd->sfd[1], &write_fds);
		
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];
			}
		}	

		tv.tv_sec = 1;
		tv.tv_usec = 0;

		retval = select(maxfd+1, NULL, &write_fds, NULL, &tv);
		
		switch(retval) {
			case 2:
				/*both real_fds are ready to perform sendmsg()*/			
				for(i=0; i<2; i++) {
					/*call to host system sendmsg()*/				
					rv[i] = sendmsg(sdfd->sfd[i], msg, flags);
					if(rv[i] == -1) {
						dif_perror("real_fd[%d] ", i);
					}
				}

				if( (rv[0] == -1) && (rv[1] == -1) )	return -1; 	/*error on the two sendmsg*/
				if( (rv[0] == -1) && (rv[1] >= 0) ) 	return rv[1];	/*error on first sendmsg, success on second*/
				if( (rv[0] >= 0)  && (rv[1] == -1) )	return rv[0];	/*success on first sendmsg, error on second*/
				if( (rv[0] >= 0)  && (rv[1] >= 0) )		return rv[0];	/*both success*/

			case 1:
				/*only one real fd is ready to perform sendmsg()*/			
				for(i=0; i<2; i++) {
					if(FD_ISSET(sdfd->sfd[i], &write_fds)) {
						index = i;
						break; /*choose the available socket*/
					}
				}	
				/*call to host system sendmsg()*/				
				rv[index] = sendmsg(sdfd->sfd[index], msg, flags);
				if(rv[index] == -1) {
					dif_perror("real_fd[%d] ", index);
				}
				return rv[index];			
			
			case -1:
				dif_perror("error on select() = %d ", retval);
				return -1;				
			break;
			
			case 0:
				return 0;
		}
		
	}
}

int umnetdif_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{

	struct sock_double_fd *sdfd=NULL;
	int rv[2], i;
	
	struct sockaddr_in *local_unbound;

	
	/*	find sockfd-key entry in hashtab*/		
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	dif_debug("sockfd %d addr %s:%d addrlen %d\n",
		sockfd,
		inet_ntoa(((struct sockaddr_in*)addr)->sin_addr),
		ntohs(((struct sockaddr_in*)addr)->sin_port),
		addrlen);

	/*probably no bind op was performed, so generic_local may be NULL*/
	/*in that case, specify INADDR_ANY as bind addr*/
	if(sdfd->generic_local == NULL) {
		/* socket is unbound, so let's bind it to INADDR_ANY:ANYPORT*/
		dif_debug("sockfd %d is not bound\n", sockfd);
		
		local_unbound = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));

		local_unbound->sin_family = AF_INET;
		local_unbound->sin_addr.s_addr = htonl(INADDR_ANY);
		local_unbound->sin_port = htons(0);
		
		rv[0] = umnetdif_bind(sockfd, (struct sockaddr*)local_unbound, sizeof(struct sockaddr_in));
		if(rv[0] == -1) {
			dif_perror("error on bind() = %d ", rv[0]);
			return -1;
		}
		else return umnetdif_connect(sockfd, addr, addrlen);
	}
	else {
		/*socket is bound*/
		uint32_t num_destaddr = ((struct sockaddr_in*)addr)->sin_addr.s_addr;		

		if(GETBNDFLG(sdfd->flags) == ANY_BOUND) {		
			/*socket bound to INADDR_ANY/VIRTIF*/
			dif_debug("sockfd %d bind flag is set to ANY_BOUND\n", sockfd);
			
			/*	reset all USE_* flags previously set by msocket().
				they will be re-set again by connect() according to destination address*/
			RESETUSGFLG(sdfd->flags, USE_MASK);
			
			if(num_destaddr == htonl(INADDR_LOOPBACK)) {
				/*calling process wants to connect this socket to 127.0.0.1*/
				
				/*	connect only first real fd
					second will not be closed for future connect() calls*/
				
				/*call to host system connect()*/
				rv[0] = connect(sdfd->sfd[0], addr, addrlen);
				if(rv[0] == -1) {
					dif_perror("real_fd[%d] ", 0);
					return -1;
				}
				else {
					/*USE_FIRST flag set again*/
					SETUSGFLG(sdfd->flags, USE_FIRST);
					if(sdfd->foreign[0] == NULL) { 
						/*this socket was not connected before*/
						sdfd->foreign[0] = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
					}
					else { 
						/*	this socket was already connected,
							but if we are here is because this is a DGRAM socket
							(in case of STREAM socket, further calls to connect() using the same socket will fail)*/
						memset((void*)sdfd->foreign[0], 0x00, sizeof(struct sockaddr_in));
					}
					
					memcpy((void*)sdfd->foreign[0], (void*)addr, addrlen);
					
					if(sdfd->generic_foreign == NULL) 
						sdfd->generic_foreign = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
					else 
						memset((void*)sdfd->generic_foreign, 0x00, sizeof(struct sockaddr_in));
						
					/*store peer address (useful for getpeername()*/
					memcpy((void*)sdfd->generic_foreign, (void*)addr, sizeof(struct sockaddr_in));
					
					dif_debug("socket %d connected, exit with success\n", sockfd);
					return rv[0];
				}
			}
			else {
				/* connect both real fds*/
				for(i=0; i<2; i++) {
					/*call to host system connect()*/
					rv[i] = connect(sdfd->sfd[i], addr, addrlen);

					if(rv[i] == -1) {
						dif_perror("real_fd[%d] ", i);
					}
					else {
						SETUSGFLG(sdfd->flags, (USE_FIRST << i));

						if(sdfd->foreign[i] == NULL) 
							sdfd->foreign[i] = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
						else 
							memset((void*)sdfd->foreign[i], 0x00, sizeof(struct sockaddr_in));
						memcpy((void*)sdfd->foreign[i], (void*)addr, addrlen);
					}				
				}
				if(sdfd->generic_foreign == NULL)
					sdfd->generic_foreign = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
				else 
					memset((void*)sdfd->generic_foreign, 0x00, sizeof(struct sockaddr_in));
					
				/*store peer address (useful for getpeername()*/	
				memcpy((void*)sdfd->generic_foreign, (void*)addr, sizeof(struct sockaddr_in));
				
				dif_debug("socket %d connected, exit with success\n", sockfd);
				if(rv[0] == 0)
					return rv[0];
				else if(rv[1] == 0)
					return rv[1];
				else
					return -1;
			}		
		}
		else if(GETBNDFLG(sdfd->flags) == LOCAL_BOUND) {
			/*socket bound to 127.0.0.1*/
			dif_debug("sockfd %d bind flag is set to LOCAL_BOUND\n", sockfd);
			
			/*	reset all USE_* flags previously set by msocket()
				they will be re-set again by connect() according to destination address*/
			RESETUSGFLG(sdfd->flags, USE_MASK);

			/*if sockfd is bound on localhost, it can only be connected to localhost */
			/* connect only first real fd*/
			/*call to host system connect()*/
			rv[0] = connect(sdfd->sfd[0], addr, addrlen);
			if(rv[0] == -1) {
				dif_perror("real_fd[%d] ", 0);
				return -1;
			}
			else {
				SETUSGFLG(sdfd->flags, USE_FIRST);
				if(sdfd->foreign[0] == NULL) {
					/*this socket was not connected before*/
					sdfd->foreign[0] = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
				}
				else {
					/*	this socket was already connected,
						but if we are here is because this is a DGRAM socket,
						in case of STREAM socket, connect will fail*/
					memset((void*)sdfd->foreign[0], 0x00, sizeof(struct sockaddr_in));
				}
				memcpy((void*)sdfd->foreign[0], (void*)addr, addrlen);
				
				if(sdfd->generic_foreign == NULL) 
					sdfd->generic_foreign = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
				else 
					memset((void*)sdfd->generic_foreign, 0x00, sizeof(struct sockaddr_in));
					
				/*store peer address (useful for getpeername()*/	
				memcpy((void*)sdfd->generic_foreign, (void*)addr, sizeof(struct sockaddr_in));
				
				dif_debug("socket %d connected, exit with success\n", sockfd);
				return rv[0];
			}			
		}
		else {
			errno = EINVAL;
			dif_perror("bad socket flag = 0x%x ", GETBNDFLG(sdfd->flags));
			return -1;		
		}
	}
}

int umnetdif_listen(int sockfd, int backlog)
{

	struct sock_double_fd *sdfd=NULL;
	int rv[2], i;
	int err_num;
	
	/*	find sockfd-key entry in hashtab*/		
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	dif_debug("sockfd %d backlog %d\n", sockfd, backlog);
	
	if(sdfd->generic_local == NULL) {
		dif_debug("socket %d is not bound\n", sockfd);
		/*socket is unbound*/
		/*let's bind it to INADDR_ANY:0*/

		struct sockaddr_in local;
	
		local.sin_family = AF_INET;
		local.sin_addr.s_addr = htonl(INADDR_ANY);
		local.sin_port = htons(0);
		
		rv[0] = umnetdif_bind(sockfd, (struct sockaddr*)&local, sizeof(struct sockaddr_in));
		if(rv[0] == -1) {
			dif_perror("error on bind() = %d ", rv[0]);
			return -1;
		}
		else return umnetdif_listen(sockfd, backlog);
	}
	else {

		if(GETBNDFLG(sdfd->flags) == LOCAL_BOUND) {
			/*in case generic_fd is bound to 127.0.0.1, 
			I will listen to the first real sock fd discarding the second*/
			dif_debug("sockfd %d bind flag is set to LOCAL_BOUND\n", sockfd);

			/*call to host system listen()*/
			rv[0] = listen(sdfd->sfd[0], backlog);
			if(rv[0] == -1) {
				dif_perror("real_fd[%d] ", 0);
				return -1;
			}	
			/*set flag to USE_FIRST*/
			SETUSGFLG(sdfd->flags, USE_FIRST);
			
			dif_debug("exit with success\n");
			return rv[0];	
		}
		else if(GETBNDFLG(sdfd->flags) == ANY_BOUND) {
			/*in case generic_fd is bound to 10.0.6.66 virtual if addr*/
			/*or is bound to INADDR_ANY, will listen to the two real socket fd*/
			dif_debug("sockfd %d bind flag is set to ANY_BOUND\n", sockfd);
			
			for(i=0; i<2; i++) {
				/*call to host system listen()*/

				rv[i] = listen(sdfd->sfd[i], backlog);
				if(rv[i] == -1) {
					dif_perror("real_fd[%d] ", i);
					err_num = errno;
				}
				SETUSGFLG(sdfd->flags, (USE_FIRST << i));
			}

			if((rv[0] == -1) && (rv[1] == -1)) {
				errno = err_num;
				return -1;
			}
			else {
				return (rv[0]==-1) ? rv[1] : rv[0];
			}
			
			/*TODO: for now, listen() returns error if and only if both calls fail
					and returns success otherwise. This policy can be changed*/			

		}
		else {
			errno = EINVAL;
			dif_perror("bad socket flag = 0x%x ", GETBNDFLG(sdfd->flags));
			return -1;
		}	
	}

}


int umnetdif_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{

	struct sock_double_fd *sdfd=NULL;

	int rv[2] = {-1, -1};
	int retval;
	int ret, i;

	/*	find sockfd-key entry in hashtab*/	
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	dif_debug("sockfd %d\n", sockfd);

	if(GETBNDFLG(sdfd->flags) == LOCAL_BOUND) {
		/*accept() returns a file descriptor for the accepted socket
		so we need to create a new socket hashtab entry where generic_fd will be
		the result of a open (on /dev/null), the first real fd will be the result of accept()
		and the second real fd will be set -1 because, in case of sockfd bound to 127.0.0.1, it will not be used.*/
		dif_debug("sockfd %d bind flag is set to LOCAL_BOUND\n", sockfd);
		
		/*create entry for file htab. This fd will be returned to the caller*/
		int generic_retfd = open("/dev/null", O_RDONLY);
		if(generic_retfd == -1) {
			dif_perror("error on open() = %d ", generic_retfd);
			return -1;
		}

		struct sock_double_fd *ret_sdfd = (struct sock_double_fd*)malloc(sizeof(struct sock_double_fd));
		memset(ret_sdfd, 0x00, sizeof(struct sock_double_fd));

		/*call to host system accept()*/
		ret_sdfd->sfd[0] = accept(sdfd->sfd[0], addr, addrlen);
		if(ret_sdfd->sfd[0] == -1) {
			dif_perror("real_fd[%d] ", 0);
			close(generic_retfd);
			free(ret_sdfd);
			return -1;
		}	
		dif_debug("accepted connection from %s:%d\n",
			inet_ntoa(((struct sockaddr_in*)addr)->sin_addr),
			ntohs(((struct sockaddr_in*)addr)->sin_port));
		
		/*set fd flag to LOCAL_BOUND according to sdfd*/
		SETTYPEFLG(ret_sdfd->flags, SOCKTYPE_STREAM);
		SETBNDFLG(ret_sdfd->flags, LOCAL_BOUND);
		SETUSGFLG(ret_sdfd->flags, USE_FIRST);
		dif_debug("sockfd %d flags 0x%x\n", sockfd, sdfd->flags);
		
		ret_sdfd->sfd[1] = -1; /*second real fd not used in case of INADDR_LOOPBACK*/
		
		ret_sdfd->local[0] = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
		ret_sdfd->foreign[0] = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
		ret_sdfd->generic_local = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
		ret_sdfd->generic_foreign = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
		
		if(addr) { /*calling process could have set addr to NULL*/
			memcpy(ret_sdfd->foreign[0], addr, sizeof(struct sockaddr_in));
			memcpy(ret_sdfd->generic_foreign, addr, sizeof(struct sockaddr_in));
		}
		/*store local address*/
		memcpy(ret_sdfd->local[0], sdfd->local[0], sizeof(struct sockaddr_in));
		memcpy(ret_sdfd->generic_local, sdfd->generic_local, sizeof(struct sockaddr_in));


		/*	insert accepted socket in socket hashtab*/
		ret = sock_htab_add(file_htab, generic_retfd, (void *)ret_sdfd);
		if(ret == -1) {
			dif_debug("Unable to add file descriptor %d to hash table\n", generic_retfd);
			close(generic_retfd);
			if(ret_sdfd->local[0])
				free(ret_sdfd->local[0]);			
			if(ret_sdfd->foreign[0])
				free(ret_sdfd->foreign[0]);
			if(ret_sdfd->generic_local)
				free(ret_sdfd->generic_local);
			if(ret_sdfd->generic_foreign)
				free(ret_sdfd->generic_foreign);
			if(ret_sdfd->subscription)
				free(ret_sdfd->subscription);
			free(ret_sdfd);	
					

			return -1;
		}
		dif_debug("return sockfd %d\n", generic_retfd);
		return generic_retfd;		
	}
	else if(GETBNDFLG(sdfd->flags) == ANY_BOUND) {
		/*sockfd is bound to INADDR_ANY / VIRTIF_ADDR (10.0.6.66)*/
		dif_debug("sockfd %d bind flag is set to ANY_BOUND\n", sockfd);
		
		fd_set read_fds;
		int maxfd = -1;
		struct timeval tv;
		int generic_retfd;	
		struct sock_double_fd *ret_sdfd = NULL;
		int index;

		
		while(TRUE) {
			FD_ZERO(&read_fds);
			FD_SET(sdfd->sfd[0], &read_fds);
			FD_SET(sdfd->sfd[1], &read_fds);
		
			if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
			if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];		
		
			tv.tv_sec = 1;
			tv.tv_usec = 0;

			retval = select(maxfd+1, &read_fds, NULL, NULL, &tv);
			
			switch(retval) {
				case 2:
					/*incoming connections from different hosts will be discarded*/
					generic_retfd = open("/dev/null", O_RDONLY);
					if(generic_retfd == -1) {
						dif_perror("error on open() = %d ", generic_retfd);
						rv[0] = -1;
						return rv[0];
					}

					ret_sdfd = (struct sock_double_fd*)malloc(sizeof(struct sock_double_fd));
					memset(ret_sdfd, 0x00, sizeof(struct sock_double_fd));
					
					struct sockaddr_in foreign[2];
					socklen_t foreign_len[2] = {sizeof(struct sockaddr_in), sizeof(struct sockaddr_in)};					

					/*call to host system accept()*/
					for(i=0; i<2; i++) {
						ret_sdfd->sfd[i] = accept(sdfd->sfd[i], (struct sockaddr*)&foreign[i], &foreign_len[i]);
						if(ret_sdfd->sfd[i] == -1) {
							dif_perror("real_fd[%d] ", i);
							close(generic_retfd);
							free(ret_sdfd);
							rv[i] = -1;
							return rv[i];
						}
						dif_debug("accepted connection from %s:%d\n",
							inet_ntoa(foreign[i].sin_addr),
							ntohs(foreign[i].sin_port));					
					}
					/*the two foreign addresses must be the same in order to successfully terminate 'accept'*/
					if(memcmp(&foreign[0].sin_addr, &foreign[1].sin_addr, sizeof(struct in_addr))) {
												
						dif_debug("incoming connections from different hosts\n");
						shutdown(ret_sdfd->sfd[0], SHUT_RDWR);
						shutdown(ret_sdfd->sfd[1], SHUT_RDWR);
						close(generic_retfd);
						free(ret_sdfd);
						return -1;
						
					}
					
								
					/*set fd flag to ANY_BOUND according to sdfd*/
					SETTYPEFLG(ret_sdfd->flags, SOCKTYPE_STREAM);
					SETBNDFLG(ret_sdfd->flags, ANY_BOUND);
					SETUSGFLG(ret_sdfd->flags, USE_BOTH);
					dif_debug("sockfd %d flags 0x%x\n", sockfd, sdfd->flags);
					
					for(i=0; i<2; i++) {
						ret_sdfd->local[i] = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
						ret_sdfd->foreign[i] = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
					}	
					ret_sdfd->generic_local = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
					ret_sdfd->generic_foreign = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
					
					if(addr) { /*fill addr with the first peer address only */
						memcpy(ret_sdfd->generic_foreign, &foreign[0], sizeof(struct sockaddr_in));
						*addrlen = sizeof(struct sockaddr_in);
					}
					memcpy(ret_sdfd->generic_local, sdfd->generic_local, sizeof(struct sockaddr_in));
										
					for(i=0; i<2; i++) {
						memcpy(ret_sdfd->foreign[i], &foreign[i], sizeof(struct sockaddr_in));
						memcpy(ret_sdfd->local[i], sdfd->local[i], sizeof(struct sockaddr_in));
					}

					/*	insert accepted socket in socket hashtab*/
					ret = sock_htab_add(file_htab, generic_retfd, (void *)ret_sdfd);
					if(ret == -1) {
						dif_debug("Unable to add file descriptor %d to hash table\n", generic_retfd);
						close(generic_retfd);
						for(i=0; i<2; i++) {
							if(ret_sdfd->local[i])
								free(ret_sdfd->local[i]);			
							if(ret_sdfd->foreign[i])
								free(ret_sdfd->foreign[i]);						
						}
						if(ret_sdfd->generic_local)
							free(ret_sdfd->generic_local);
						if(ret_sdfd->generic_foreign)
							free(ret_sdfd->generic_foreign);
						if(ret_sdfd->subscription)
							free(ret_sdfd->subscription);							
						free(ret_sdfd);			
						rv[0] = -1;

						return rv[0];
					}
					rv[0] = generic_retfd;

					dif_debug("return sockfd %d\n", generic_retfd);
					return rv[0];

				case 1:
					/*only one incoming connection, it will be returned to caller*/
					for(i=0; i<2; i++)
						if(FD_ISSET(sdfd->sfd[i], &read_fds))
							index = i;
				
					generic_retfd = open("/dev/null", O_RDONLY);
					if(generic_retfd == -1) {
						dif_perror("error on open() = %d ", generic_retfd);
						rv[0] = -1;
						return rv[0];
					}	

					ret_sdfd = (struct sock_double_fd*)malloc(sizeof(struct sock_double_fd));
					memset(ret_sdfd, 0x00, sizeof(struct sock_double_fd));

					/*call to host system accept()*/
					ret_sdfd->sfd[0] = accept(sdfd->sfd[index], addr, addrlen);
					if(ret_sdfd->sfd[0] == -1) {
						dif_perror("real_fd[%d] ", index);
						close(generic_retfd);
						free(ret_sdfd);
						rv[0] = -1;
						return rv[0];
					}	
					dif_debug("accepted connection from %s:%d\n",
						inet_ntoa(((struct sockaddr_in*)addr)->sin_addr),
						ntohs(((struct sockaddr_in*)addr)->sin_port));
					
					/*set fd flag to ANY_BOUND according to sdfd*/
					SETTYPEFLG(ret_sdfd->flags, SOCKTYPE_STREAM);
					SETBNDFLG(ret_sdfd->flags, ANY_BOUND);
					SETUSGFLG(ret_sdfd->flags, USE_FIRST);
					dif_debug("sockfd %d flags 0x%x\n", sockfd, sdfd->flags);
					
					ret_sdfd->sfd[1] = -1; /*second real fd not used*/			
					ret_sdfd->local[0] = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
					ret_sdfd->foreign[0] = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
					ret_sdfd->generic_local = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
					ret_sdfd->generic_foreign = (struct sockaddr*)malloc(sizeof(struct sockaddr_in));
		
					if(addr) {
						memcpy(ret_sdfd->foreign[0], addr, sizeof(struct sockaddr_in));
						memcpy(ret_sdfd->generic_foreign, addr, sizeof(struct sockaddr_in));
					}
					/*store local address*/
					memcpy(ret_sdfd->local[0], sdfd->local[0], sizeof(struct sockaddr_in));
					memcpy(ret_sdfd->generic_local, sdfd->generic_local, sizeof(struct sockaddr_in));
				
					/*	insert accepted socket in socket hashtab*/
					ret = sock_htab_add(file_htab, generic_retfd, (void *)ret_sdfd);
					if(ret == -1) {
						dif_debug("Unable to add file descriptor %d to hash table\n", generic_retfd);
						close(generic_retfd);
						if(ret_sdfd->local[0])
							free(ret_sdfd->local[0]);			
						if(ret_sdfd->foreign[0])
							free(ret_sdfd->foreign[0]);
						if(ret_sdfd->generic_local)
							free(ret_sdfd->generic_local);
						if(ret_sdfd->generic_foreign)
							free(ret_sdfd->generic_foreign);
						if(ret_sdfd->subscription)
							free(ret_sdfd->subscription);							
						free(ret_sdfd);			
						rv[0] = -1;

						return rv[0];
					}
					rv[0] = generic_retfd;

					dif_debug("return sockfd %d\n", generic_retfd);
					return rv[0];

				case -1:
					/*select() error*/
					dif_perror("error on select() = %d ", retval);
					return -1;

			}
		}
	}
	else {
		errno = EINVAL;
		dif_perror("bad socket flag = 0x%x ", GETBNDFLG(sdfd->flags));
		return -1;	
	}	
}


ssize_t umnetdif_read(int fd, void *buf, size_t count)
{
	int rv[2], i, index;
	int retval;

	struct sock_double_fd *sdfd=NULL;
	
	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, fd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", fd);
		return -1;
	}
	dif_debug("fd %d buf_addr 0x%x count %d\n", fd, buf, count);
	
	fd_set read_fds;
	int maxfd = -1;
	struct timeval tv;
	
	/*temporary buffers*/
	char buffer[2][count];
	memset(&buffer[0][0], 0x00, count);
	memset(&buffer[1][0], 0x00, count);
	
	while(TRUE) {	
	
		if(GETBNDFLG(sdfd->flags) == LOCAL_BOUND) {	
			/*sdfd is bound to 127.0.0.1, so only first real_fd will be used to perform read()*/
			dif_debug("sockfd %d bind flag is set to LOCAL_BOUND\n", fd);
			
			maxfd = -1;
			FD_ZERO(&read_fds);
			FD_SET(sdfd->sfd[0], &read_fds);
			
			if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
		}
		else if(GETBNDFLG(sdfd->flags) == ANY_BOUND) {
			/*sdfd is bound to INADDR_ANY / VIRTIF_ADDR (10.0.6.66)*/
			dif_debug("sockfd %d bind flag is set to ANY_BOUND\n", fd);
			
			if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
				/*sdfd is marked as USE_BOTH, so both real_fds will be used to perform read()*/
				dif_debug("sockfd %d usage flag is set to USE_BOTH\n", fd);
				
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[0], &read_fds);
				FD_SET(sdfd->sfd[1], &read_fds);
	
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];	
			}

			if(GETUSGFLG(sdfd->flags) == USE_FIRST) {
				/*sdfd is marked as USE_FIRST, so only first real_fd will be used to perform read()*/
				dif_debug("sockfd %d usage flag is set to USE_FIRST\n", fd);
				
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[0], &read_fds);
			
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
			}

			if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
				/*sdfd is marked as USE_SECOND, so only second real_fd will be used to perform read()*/
				dif_debug("sockfd %d usage flag is set to USE_SECOND\n", fd);
				
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[1], &read_fds);
		
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];
			}
		}
	

		tv.tv_sec = 1;
		tv.tv_usec = 0;

		retval = select(maxfd+1, &read_fds, NULL, NULL, &tv);
		
		switch(retval) {
			case 2:
				dif_debug("socket %d and socket %d are ready to perform read()\n", sdfd->sfd[0], sdfd->sfd[1]);
				/*both real_fds are ready to perform read()*/
				for(i=0; i<2; i++) {
					/*call to host system read()*/
					rv[i] = read(sdfd->sfd[i], &buffer[i][0], count);
					if(rv[i] == -1) {
						dif_perror("real_fd[%d] ", i);
					}

				}
				
				
				if( (rv[0] == -1) && (rv[1] == -1) ) {
					dif_debug("rv[0] = %d rv[1] = %d\n", rv[0], rv[1]);
					return -1; /*error on the two reads*/
				}
				if( (rv[0] >= 0) && (rv[1] >= 0) ) {			/*both success*/
					dif_debug("rv[0] = %d rv[1] = %d\n", rv[0], rv[1]);
					
					if(memcmp(&buffer[0][0], &buffer[1][0], count) == 0) {	/*if buffers match, return first (or second) buffer*/
						dif_debug("buffers match %d\n", count);
						memcpy(buf, &buffer[0][0], rv[0]);
						return rv[0];
					}
					else /*TODO: for now we decide to return error. This policy can be changed*/
						return -1;
			
				}
				
				index = (rv[0] == -1) ? 1 : 0;
				
				dif_debug("rv[0] = %d rv[1] = %d\n", rv[0], rv[1]);
				memcpy(buf, &buffer[index][0], rv[index]);
				return rv[index];

			
			case 1:
				/*only one of the two real_fds is ready to perform read()*/
				for(i=0; i<2; i++) {
					if(FD_ISSET(sdfd->sfd[i], &read_fds)) {
						index = i;
						break; /* choose the available socket*/
					}
				}	
				/*call to host system read()*/
				rv[index] = read(sdfd->sfd[index], buf, count);
				if(rv[index] == -1) {
					dif_perror("real_fd[%d] ", index);
				}
				if(rv[index] == 0) {
					dif_debug("received EOF on real_fd[%d] = %d\n", index, sdfd->sfd[index]);

					memset(buf, 0x00, count);
					if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
						RESETUSGFLG(sdfd->flags, (USE_FIRST<<index));
						shutdown(sdfd->sfd[index], SHUT_RDWR);
						close(sdfd->sfd[index]);
						if(sdfd->local[index])
							free(sdfd->local[index]);
						if(sdfd->foreign[index])
							free(sdfd->foreign[index]);
						return 1;						
					}
					else if((GETUSGFLG(sdfd->flags) == USE_FIRST) || (GETUSGFLG(sdfd->flags) == USE_SECOND)) {

						return 0;
					}
				}
				return rv[index];			
			break;
			
			case -1:
				/*select() error*/
				dif_perror("error on select() = %d ", retval);
				return -1;				
			break;
			case 0:
				return -1;				
			break;			
		}
		
	}	

}


ssize_t umnetdif_recvfrom(int sockfd, void *buf, size_t len, int flags,
					struct sockaddr *src_addr, socklen_t *addrlen)
{
	int rv[2], i, index;
	int retval;

	struct sock_double_fd *sdfd=NULL;
	
	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	dif_debug("sockfd %d buf_addr 0x%x len %d flags 0x%x\n", sockfd, buf, len, flags);

	fd_set read_fds;
	int maxfd = -1;
	struct timeval tv;
	
	/*temporary buffers*/
	char buffer[2][len];
	memset(&buffer[0][0], 0x00, len);
	memset(&buffer[1][0], 0x00, len);
	
	struct sockaddr_in source[2];
	socklen_t source_len[2];
	
	if(src_addr && addrlen) { /*they could be both NULL*/
		memset(&source[0], 0x00, sizeof(struct sockaddr_in));
		memset(&source[1], 0x00, sizeof(struct sockaddr_in));	
		source_len[0] = source_len[1] = *addrlen;
		dif_debug("receiving from %s:%d\n",
			inet_ntoa(((struct sockaddr_in*)src_addr)->sin_addr),
			ntohs(((struct sockaddr_in*)src_addr)->sin_port));
	}

	
	while(TRUE) {	
		
		if(GETBNDFLG(sdfd->flags) == LOCAL_BOUND) {
			/*sdfd is bound to 127.0.0.1*/
			dif_debug("sockfd %d bind flag is set to LOCAL_BOUND\n", sockfd);
			
			maxfd = -1;
			FD_ZERO(&read_fds);
			FD_SET(sdfd->sfd[0], &read_fds);
			
			if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
		}
		else if(GETBNDFLG(sdfd->flags) == ANY_BOUND) {
			/*sdfd is bound to INADDR_ANY or VIRTIF_ADDR (10.0.6.66)*/
			dif_debug("sockfd %d bind flag is set to ANY_BOUND\n", sockfd);
			
			if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
				/*sdfd is marked as USE_BOTH, so both real_fds will be used to perform recvfrom()*/
				dif_debug("sockfd %d usage flag is set to USE_BOTH\n", sockfd);
				
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[0], &read_fds);
				FD_SET(sdfd->sfd[1], &read_fds);
	
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];	
			}

			if(GETUSGFLG(sdfd->flags) == USE_FIRST) {
				/*sdfd is marked as USE_FIRST, so only first real_fd will be used to perform recvfrom()*/
				dif_debug("sockfd %d usage flag is set to USE_FIRST\n", sockfd);
				
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[0], &read_fds);
			
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
			}

			if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
				/*sdfd is marked as USE_SECOND, so only second real_fd will be used to perform recvfrom()*/
				dif_debug("sockfd %d usage flag is set to USE_SECOND\n", sockfd);
				
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[1], &read_fds);
		
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];
			}
		}
	
		tv.tv_sec = 1;
		tv.tv_usec = 0;

		retval = select(maxfd+1, &read_fds, NULL, NULL, &tv);
		
		switch(retval) {
			case 2:
				/*both real_fds are ready to perform recvfrom()*/
				for(i=0; i<2; i++) {
					/*call to host system recvfrom()*/
					if(src_addr && addrlen) { /*if caller did not set them to NULL*/
						rv[i] = recvfrom(sdfd->sfd[i], &buffer[i][0], len, flags, (struct sockaddr*)&source[i], &source_len[i]);
					}
					else {
						rv[i] = recvfrom(sdfd->sfd[i], &buffer[i][0], len, flags, NULL, NULL);
					}

					if(rv[i] == -1) {
						dif_perror("real_fd[%d] ", i);
					}
				}
				
				if( (rv[0] == -1) && (rv[1] == -1) ) {
					return -1; /*error on the two recvfrom*/
				}
				
				if( (rv[0] >= 0) && (rv[1] >= 0) ) {			/*both success*/
				
					if(memcmp(&buffer[0][0], &buffer[1][0], len) == 0) {
						if(src_addr && addrlen) {
							if(memcmp(&source[0].sin_addr, &source[1].sin_addr, sizeof(struct in_addr)) != 0) {
								dif_debug("addresses do not match\n");
								return -1;
							}
							else {
								memcpy(src_addr, &source[0], source_len[0]);
								*addrlen = source_len[0];
							}
						}
						memcpy(buf, &buffer[0][0], rv[0]);
						return rv[0];						
					}
					else {
						dif_debug("buffers do not match\n");
						return -1;
					}
				}	
				
				index = (rv[0] == -1) ? 1 : 0;
				
				memcpy(buf, &buffer[index][0], rv[index]);
				if(src_addr && addrlen) {
					memcpy(src_addr, (struct sockaddr*)&source[index], source_len[index]);
					*addrlen = source_len[index];
				}
				return rv[index];
				
			
			case 1:
				/*only one of the two real_fds is ready to perform recvfrom()*/
				for(i=0; i<2; i++) {
					if(FD_ISSET(sdfd->sfd[i], &read_fds)) {
						index = i;
						break; /* choose the available socket*/
					}
				}	
				/*call to host system recvfrom()*/
				rv[index] = recvfrom(sdfd->sfd[index], buf, len, flags, src_addr, addrlen);
				if(rv[index] == -1) {
					dif_perror("real_fd[%d] ", index);
				}
				if(rv[index] == 0) {
					dif_debug("received EOF on real_fd[%d] = %d\n", index, sdfd->sfd[index]);

					memset(buf, 0x00, len);
					if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
						RESETUSGFLG(sdfd->flags, (USE_FIRST<<index));
						shutdown(sdfd->sfd[index], SHUT_RDWR);
						close(sdfd->sfd[index]);
						if(sdfd->local[index])
							free(sdfd->local[index]);
						if(sdfd->foreign[index])
							free(sdfd->foreign[index]);
						return 1;						
					}
					else if((GETUSGFLG(sdfd->flags) == USE_FIRST) || (GETUSGFLG(sdfd->flags) == USE_SECOND)) {

						return 0;
					}
				}				
				return rv[index];			
			
			case -1:
				/*select() error*/
				dif_perror("error on select() = %d ", retval);
				return -1;				
			case 0:
				return -1;				
		}
		
	}	
}


ssize_t umnetdif_recvmsg(int sockfd, struct msghdr *msg, int flags)
{
	int rv[2], i, index;
	int retval;

	struct sock_double_fd *sdfd=NULL;
	
	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	
	fd_set read_fds;
	int maxfd = -1;
	struct timeval tv;

	
	while(TRUE) {	
	
		if(GETBNDFLG(sdfd->flags) == LOCAL_BOUND) {
			/*sdfd is bound to 127.0.0.1*/
			maxfd = -1;
			FD_ZERO(&read_fds);
			FD_SET(sdfd->sfd[0], &read_fds);
			
			if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
		}
		else if(GETBNDFLG(sdfd->flags) == ANY_BOUND) {
			/*sdfd is bound to INADDR_ANY or VIRTIF_ADDR (10.0.6.66)*/
			if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
				/*sdfd is marked as USE_BOTH, so both real_fds will be used to perform recvmsg()*/
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[0], &read_fds);
				FD_SET(sdfd->sfd[1], &read_fds);
	
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];	
			}
			
			if(GETUSGFLG(sdfd->flags) == USE_FIRST) {
				/*sdfd is marked as USE_FIRST, so only first real_fd will be used to perform recvmsg()*/
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[0], &read_fds);
			
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
			}

			if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
				/*sdfd is marked as USE_SECOND, so only second real_fd will be used to perform recvmsg()*/
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[1], &read_fds);
		
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];
			}
		}
	
		tv.tv_sec = 1;
		tv.tv_usec = 0;

		retval = select(maxfd+1, &read_fds, NULL, NULL, &tv);
		
		switch(retval) {
			case 2:
				
				/*TODO: there is no support for select() returning 2 (both socket ready to recv)*/
				errno = EINVAL;

				return -1;
			
			case 1:
				/*one of the two real_fds is ready to perform recvmsg()*/
				for(i=0; i<2; i++) {
					if(FD_ISSET(sdfd->sfd[i], &read_fds)) {
						index = i;
						break; /* choose the available socket*/
					}
				}	
				/*call to host system recvmsg()*/
				rv[index] = recvmsg(sdfd->sfd[index], msg, flags);
				if(rv[index] == -1) {
					dif_perror("real_fd[%d] ", index);
				}
				return rv[index];			
			
			case -1:
				/*select() error*/
				dif_perror("error on select() = %d ", retval);
				return -1;				

			case 0:
				return -1;				

		}
	}	
}

ssize_t umnetdif_recv(int sockfd, void *buf, size_t len, int flags)
{
	int rv[2], i, index;
	int retval;

	struct sock_double_fd *sdfd=NULL;
	
	/*	find sockfd-key entry in hashtab*/
	sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, sockfd);
	if(!sdfd) {
		errno = EBADF;
		dif_perror("Unable to get file descriptor %d from hashtable ", sockfd);
		return -1;
	}
	dif_debug("sockfd %d buf_addr 0x%x len %d flags 0x%x\n", sockfd, buf, len, flags);

	fd_set read_fds;
	int maxfd = -1;
	struct timeval tv;
	
	/*temporary buffers*/
	char buffer[2][len];
	memset(&buffer[0][0], 0x00, len);
	memset(&buffer[1][0], 0x00, len);
	
	while(TRUE) {	
		
		if(GETBNDFLG(sdfd->flags) == LOCAL_BOUND) {
			/*sdfd is bound to 127.0.0.1*/
			dif_debug("sockfd %d bind flag is set to LOCAL_BOUND\n", sockfd);
			
			maxfd = -1;
			FD_ZERO(&read_fds);
			FD_SET(sdfd->sfd[0], &read_fds);
			
			if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
		}
		else if(GETBNDFLG(sdfd->flags) == ANY_BOUND) {
			/*sdfd is bound to INADDR_ANY or VIRTIF_ADDR (10.0.6.66)*/
			dif_debug("sockfd %d bind flag is set to ANY_BOUND\n", sockfd);
			
			if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
				/*sdfd is marked as USE_BOTH, so both real_fds will be used to perform recv()*/
				dif_debug("sockfd %d usage flag is set to USE_BOTH\n", sockfd);
				
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[0], &read_fds);
				FD_SET(sdfd->sfd[1], &read_fds);
	
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];	
			}

			if(GETUSGFLG(sdfd->flags) == USE_FIRST) {
				/*sdfd is marked as USE_FIRST, so only first real_fd will be used to perform recv()*/
				dif_debug("sockfd %d usage flag is set to USE_FIRST\n", sockfd);
				
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[0], &read_fds);
			
				if(maxfd < sdfd->sfd[0]) maxfd = sdfd->sfd[0];
			}
			if(GETUSGFLG(sdfd->flags) == USE_SECOND) {
				/*sdfd is marked as USE_SECOND, so only second real_fd will be used to perform recv()*/
				dif_debug("sockfd %d usage flag is set to USE_SECOND\n", sockfd);
				
				maxfd = -1;
				FD_ZERO(&read_fds);
				FD_SET(sdfd->sfd[1], &read_fds);
		
				if(maxfd < sdfd->sfd[1]) maxfd = sdfd->sfd[1];
			}
		}
	
		tv.tv_sec = 1;
		tv.tv_usec = 0;

		retval = select(maxfd+1, &read_fds, NULL, NULL, &tv);
		
		switch(retval) {
			case 2:
				/*both real_fds are ready to perform recv()*/
				for(i=0; i<2; i++) {
					/*call to host system recv()*/
					rv[i] = recv(sdfd->sfd[i], &buffer[i][0], len, flags);
					if(rv[i] == -1) {
						dif_perror("real_fd[%d] ", i);
					}
				}

				if( (rv[0] == -1) && (rv[1] == -1) ) {
					return -1; /*error on the two recv*/
				}
				if( (rv[0] >= 0) && (rv[1] >= 0) ) {			/*both success*/
					if(memcmp(&buffer[0][0], &buffer[1][0], len) == 0) {	/*if buffers match, return first (or second) buffer*/
						memcpy(buf, &buffer[0][0], rv[0]);
						return rv[0];
					}
					else /*TODO: for now we decide to return error. This policy can be changed*/
						return -1;					
				}
				
				index = (rv[0] == -1) ? 1 : 0;

				dif_debug("rv[0] = %d rv[1] = %d\n", rv[0], rv[1]);
				memcpy(buf, &buffer[index][0], rv[index]);
				return rv[index];

			
			case 1:
				/*only one of the two real_fds is ready to perform recv()*/
				for(i=0; i<2; i++) {
					if(FD_ISSET(sdfd->sfd[i], &read_fds)) {
						index = i;
						break; /* choose the available socket */
					}
				}	
				/*call to host system recv()*/
				rv[index] = recv(sdfd->sfd[index], buf, len, flags); 
				if(rv[index] == -1) {
					dif_perror("real_fd[%d] ", index);
				}
				if(rv[index] == 0) {
					dif_debug("received EOF on real_fd[%d] = %d\n", index, sdfd->sfd[index]);

					memset(buf, 0x00, len);
					if(GETUSGFLG(sdfd->flags) == USE_BOTH) {
						RESETUSGFLG(sdfd->flags, (USE_FIRST<<index));
						shutdown(sdfd->sfd[index], SHUT_RDWR);
						close(sdfd->sfd[index]);
						if(sdfd->local[index])
							free(sdfd->local[index]);
						if(sdfd->foreign[index])
							free(sdfd->foreign[index]);
						return 1;						
					}
					else if((GETUSGFLG(sdfd->flags) == USE_FIRST) || (GETUSGFLG(sdfd->flags) == USE_SECOND)) {

						return 0;
					}
				}				
				return rv[index];			
			
			case -1:
				/*select() error*/
				dif_perror("error on select() = %d ", retval);
				return -1;				

			case 0:
				return -1;				
		}
	}	
}




struct umnet_operations umnet_ops={
	.msocket=umnetdif_msocket, 			
	.bind=umnetdif_bind,					
	.connect=umnetdif_connect,
	.listen=umnetdif_listen,				
	.accept=umnetdif_accept,				
	.getsockname=umnetdif_getsockname, 	 
	.getpeername=umnetdif_getpeername,		
	.send=umnetdif_send,					
	.sendto=umnetdif_sendto,				
	.recv=umnetdif_recv,				
	.recvfrom=umnetdif_recvfrom,			
	.sendmsg=umnetdif_sendmsg,				
	.recvmsg=umnetdif_recvmsg,				
	.getsockopt=umnetdif_getsockopt,		
	.setsockopt=umnetdif_setsockopt,		
	.read=umnetdif_read,					
	.write=umnetdif_write,	
	.shutdown=umnetdif_shutdown,				
/*	.ioctl=umnetdif_ioctl,*/
	.close=umnetdif_close,					
/*	.ioctlparms=umnetdif_ioctlparms,*/
	.init=umnetdif_init,
	.fini=umnetdif_fini,
	.event_subscribe=umnetdif_event_subscribe,
	.supported_domain=umnetdif_supported_domain
};
