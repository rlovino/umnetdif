/*   Copyright 2014 Raffaele Lovino <raff.lovino@gmail.com>
 *   
 *   This is part of umNETDIF
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License, version 2, as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
 *
 */

#ifndef _UMNETDIF_TYPES_H
#define _UMNETDIF_TYPES_H

#include "umnet.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/*struct for handling double fd*/
struct sock_double_fd {
	int sfd[2];
	unsigned int flags;
	struct sockaddr *generic_local; /*socket's local address as seen by application running inside umview*/
	struct sockaddr *generic_foreign; /*socket's peer address as seen by application running inside umview (if socket is connected)*/
	struct sockaddr *local[2]; /*real sockets' local addresses as seen by umnetdif*/
	struct sockaddr *foreign[2]; /*real sockets' peer addresses as seen by umnetdif*/
	struct event_subs_arg *subscription;
};


/*struct for interface status*/
struct ifstatus {
	char name[16];
	char ipv4addr[16];
};

/*event subscription structure*/
struct event_subs_arg {
	voidfun cb;
	void *arg;
	int fd;
	int how;
};

#endif /*_UMNETDIF_TYPES_H*/
