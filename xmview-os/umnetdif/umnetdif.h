/*   Copyright 2014 Raffaele Lovino <raff.lovino@gmail.com>
 *   
 *   This is part of umNETDIF
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License, version 2, as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
 *
 */

#ifndef _UMNETDIF_H
#define _UMNETDIF_H

#include "umnetdif_types.h"


#define MAX_OPEN_FILENO 1024
#define FILE_HTAB_SIZE 	MAX_OPEN_FILENO + (MAX_OPEN_FILENO/2)

#define TRUE 1
#define FALSE 0

#define FD_STRING_SIZE 8


/*double socket flags and masks*/
#define LOCAL_BOUND 	0x00000001
#define ANY_BOUND	0x00000002
#define BIND_MASK	0x00000003

#define USE_MASK	0x0000000C
#define USE_FIRST	0x00000004
#define USE_SECOND	0x00000008
#define USE_BOTH	0x0000000C

#define SOCKTYPE_MASK	0xC0000000
#define SOCKTYPE_DGRAM	0x80000000
#define SOCKTYPE_STREAM	0x40000000

/*flag handling macros*/
#define GETFLAG(sf,m)		(sf & m)
#define SETFLAG(sf,f)		(sf |= f)
#define RESETFLAG(sf,f)		(sf ^= f)

#define GETBNDFLG(sf)		GETFLAG(sf, BIND_MASK)
#define SETBNDFLG(sf,f)		SETFLAG(sf,f)
#define RESETBNDFLG(sf,f)	RESETFLAG(sf,f)

#define GETUSGFLG(sf)		GETFLAG(sf, USE_MASK)
#define SETUSGFLG(sf,f)		SETFLAG(sf,f)
#define RESETUSGFLG(sf,f)	RESETFLAG(sf,f)

#define GETTYPEFLG(sf)		GETFLAG(sf, SOCKTYPE_MASK)
#define SETTYPEFLG(sf,f)	SETFLAG(sf,f)
#define RESETTYPEFLG(sf,f)	RESETFLAG(sf,f)

/*other macros*/
#define max(a,b) \
	({ __typeof__ (a) _a = (a); \
		__typeof__ (b) _b = (b); \
		_a > _b ? _a : _b; })

#define min(a,b) \
	({ __typeof__ (a) _a = (a); \
		__typeof__ (b) _b = (b); \
		_a < _b ? _a : _b; })


/*debug mode macros*/		
#ifndef DEBUG
#define DEBUG 0
#endif

#ifndef POLL_DEBUG
#define POLL_DEBUG 1
#endif
		
#define dif_debug(fmt, ...) \
        do { if (DEBUG) fprintf(stderr, "DEBUG %s:%d: %s(): " fmt, __FILE__, \
                                __LINE__, __func__, ##__VA_ARGS__); } while (0)
                                
#define dif_perror(fmt, ...) \
        do { if (DEBUG) { fprintf(stderr, "ERROR %s:%d: %s(): ERROR: " fmt, __FILE__, \
                                __LINE__, __func__, ##__VA_ARGS__); perror("");} } while (0)		
		


/*global structs: one for each interface, statically allocated*/
struct ifstatus interface[2];



/*poll thread*/
pthread_t poll_thread;
pthread_attr_t poll_thread_attr;
/*poll thread command pipe */
int poll_command_rdpipe;
int poll_command_wrpipe;
/*poll thread data pipe */
int poll_data_rdpipe;
int poll_data_wrpipe;
/*poll thread synchronization*/
pthread_mutex_t poll_mutex;
pthread_cond_t poll_cv;


/*pointer to socket descriptors hash table*/
void *file_htab;



/*virtual interface IPv4 address (there is no virtual interface for now)*/
struct in_addr virt_if_ipv4_addr;
uint32_t virt_if_ipv4_num_addr;


#endif
