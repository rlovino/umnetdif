/*   Copyright 2014 Raffaele Lovino <raff.lovino@gmail.com>
 *   
 *   This is part of umNETDIF
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License, version 2, as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
 *
 */

#include <poll.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "poll_utils.h"
#include "umnetdif.h"



int umnetdif_poll(struct pollfd *fds, nfds_t nfds, int timeout)
{
	int num_elem = (int)nfds;
	int i, j;
	struct sock_double_fd *sdfd = NULL;
	struct pollfd my_fds[2][num_elem];
	int retval[2];
	int count = 0;
	
	for(i=0; i<num_elem; i++) {
		/*	find sockfd-key entry in hashtab*/
		sdfd = (struct sock_double_fd*)sock_htab_get(file_htab, fds[i].fd);
		if(!sdfd) {
		/*this could be a problem*/
			/*fprintf(stderr, "\nDIF: umnetdif_poll(): Unable to get file descriptor %d from hashtable\n",
					fds[i].fd);*/
			return -1;
		}
		else {
			my_fds[0][i].fd = sdfd->sfd[0];
			my_fds[0][i].events = fds[i].events;
			my_fds[0][i].revents = fds[i].revents;				
	
			my_fds[1][i].fd = sdfd->sfd[1];
			my_fds[1][i].events = fds[i].events;
			my_fds[1][i].revents = fds[i].revents;	
		}
	}


	/*	there is no need to distinguish between local or anybind
		since poll() called on fd = -1 sets revents field to 0*/
	for(j=0; j<2; j++) {
		retval[j] = poll(my_fds[j], nfds, timeout);
		if(retval[j] == -1) {
			perror("DIF: poll()");
			return -1;
		}
	}
	
	for(i=0; i<num_elem; i++) {
		/*projection of results*/
		fds[i].revents = my_fds[0][i].revents | my_fds[1][i].revents;
		
		/*count for matches*/
		if((fds[i].events & fds[i].revents) == fds[i].events) count++;
	}


	return count;
}


/*poll array handling*/
nfds_t poll_array_init(struct pollfd *fds, int n)
{
	int i;
	for(i=0; i<n; i++) {
		fds[i].fd = -1;
		fds[i].events = fds[i].revents = 0;
	}
	return 0;
}


nfds_t poll_array_add(struct pollfd *fds, struct pollfd entry, nfds_t dim)
{
	memcpy((void*) &fds[dim], (void*) &entry, sizeof(struct pollfd));
	return dim+1;
}

nfds_t poll_array_remove(struct pollfd *fds, int fd, nfds_t dim)
{
	nfds_t i, j;
	nfds_t index = -1;

	if(dim == 0) return 0;
	else {
		for(i=0; i<dim; i++) {
			if(fds[i].fd == fd) {
				index = i;
				break;
			}
		}
	
		if(index == -1) return dim;
		else {
			memmove((void*) &fds[index], (void*) &fds[index+1], (MAX_OPEN_FILENO-index-1)*sizeof(struct pollfd));
			fds[MAX_OPEN_FILENO-1].fd = -1;
			fds[MAX_OPEN_FILENO-1].events = fds[MAX_OPEN_FILENO-1].revents = 0;
			return dim-1;
		}	
	}	

}

void poll_array_print(struct pollfd *fds, nfds_t dim)
{
	nfds_t i;
	
	fprintf(stderr, "index\tgen_fd\tevents\trevents\n");
	for(i=0; i<dim; i++) {
		fprintf(stderr, "%d:\t%d\t%d\t%d\t\n", i, fds[i].fd, fds[i].events, fds[i].revents);
	}
	fprintf(stderr, "****************** DIM %d\n\n", dim);	
}

