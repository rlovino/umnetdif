/*   Copyright 2014 Raffaele Lovino <raff.lovino@gmail.com>
 *   
 *   This is part of umNETDIF
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License, version 2, as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
 *
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <poll.h>
#include <linux/if.h>
#include <sys/ioctl.h>
#include <stdint.h>

#include "umnetdif_utils.h"
#include "umnetdif_types.h"
#include "umnetdif.h"
#include "sock_htab.h"




char *int_to_string(int fd)
{
	static char fd_string[FD_STRING_SIZE];
	
	memset(fd_string, 0x00, sizeof(fd_string));
	sprintf(fd_string, "%d", fd);
	return fd_string;
}

int get_virt_if_ipv4_addr(struct in_addr *inp)
{
	int rc=0;
	memset(inp, 0x00, sizeof(struct in_addr));
	rc = inet_aton("10.0.6.66", inp);
	if(rc > 0) 
		return rc;
	else 
		return -1;
}

void get_interface_ipv4_addr(char *ifname, char *ipv4_addr_ptr)
{
	int sockfd;
	struct ifreq ifr;
	char *name;
	int retval;
	
	name = strdup(ifname);
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, name, IFNAMSIZ);
	
	retval = ioctl(sockfd, SIOCGIFADDR, &ifr);
	if(retval == -1) {
		dif_perror("error on ioctl() = %d ", retval);
		return;
	}

	sprintf(ipv4_addr_ptr, "%s", inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr));

	close(sockfd);
	return;
}

uint16_t get_rand_port()
{
/*this function is not well tested, it needs to be substituted with something better*/
	int min = 1024;
	int max = 65535;
	uint16_t port;
	struct timeval tv;
	
	gettimeofday(&tv, NULL);
	
	srand(tv.tv_usec);
	
	port = max + rand() / (RAND_MAX / (max - min + 1) + 1);
	if(port < 1024)
		port += 1024;
	return port;
}

int port_is_free(uint32_t addr, uint16_t port, int type)
{
	struct sockaddr_in local[2];
	int sockfd[2];
	int optval[2] = {1, 1};
	int rv[2], i;
	int ret;

	switch(addr) {
		case INADDR_ANY:
			for(i=0; i<2; i++) {
				sockfd[i] = socket(AF_INET, type, 0);
				if(sockfd[i] == -1) {
					dif_perror("error on socket() %d ", i);
					return -1;
				}	
				
				ret = setsockopt(sockfd[i], SOL_SOCKET, SO_REUSEADDR, (void*)&optval[i], sizeof(optval[i]));
				if(ret == -1) {
					dif_perror("error on setsockopt() %d ", i);
					return -1;
				}
					
				local[i].sin_family = AF_INET;    
				local[i].sin_port = htons(port);
				inet_pton(AF_INET, interface[i].ipv4addr, &local[i].sin_addr);
			    
				/*it uses bind to test whether a port is free or not*/
				ret = bind(sockfd[i], (struct sockaddr *)&local[i], sizeof(struct sockaddr_in));
				if(ret == -1) {
					dif_perror("error on bind() %d ", i);
					close(sockfd[i]);
					rv[i] = 0;
				}
				else {
					close(sockfd[i]);
					rv[i] = 1;
				}				
					
			}
			return (rv[0] * rv[1]);
 
		
		case INADDR_LOOPBACK:
			sockfd[0] = socket(AF_INET, type, 0);
			if(sockfd[0] == -1) {
				dif_perror("error on socket() = %d ", sockfd[0]);
				return -1;
			}

			local[0].sin_family = AF_INET;    
			local[0].sin_port = htons(port);  
			inet_pton(AF_INET, "127.0.0.1", &local[0].sin_addr);
			ret = bind(sockfd[0], (struct sockaddr *)&local[0], sizeof(struct sockaddr_in));
			if(ret == -1) {
				dif_perror("error on bind() = %d ", ret);
				close(sockfd[0]);
				rv[0] = 0;
				return rv[0];
			}
			else {
				close(sockfd[0]);
				rv[0] = 1;
				return rv[0];
        		}

	}
}



