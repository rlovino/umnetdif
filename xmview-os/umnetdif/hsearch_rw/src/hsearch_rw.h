//
// hsearch_rw: A wrapper of hsearch_r coming with more practical functions.
// Copyright 2013 coronocoya.net <nshou@coronocoya.net>
//
// This program can be distributed under the terms of MIT License.
// See the file COPYING.
//

#pragma once
#include <sys/types.h>


////////
// Note
// Neither keys nor values are copied when inserted into hsearch_rw, so they must exist for the lifetime of the hserch_rw.
// This means that the use of static strings for keys is OK, but temporary strings (i.e. those created in function stacks) 
// should be copied with 'strdup()' before being inserted as keys. If keys or values are dynamically allocated, you must be 
// careful to ensure that they are freed when they are removed from the hsearch_rw.
// These functions are reentrant and thread-safe as far as the different 'hsearch_rw' instances are used. If the same 
// instance can be accessed by multiple threads, they should be used under the exclusive control.


////////
// Handle for a hash table of hsearch_rw.
struct hsearch_rw;


////////
// Key and value structure standing for one item in the hash table.
struct hsearch_rw_item{
  char *key;
  void *val;
};


////////
// Create new hash table of hsearch_rw. Returns its pointer if it is successfully created, or NULL.
struct hsearch_rw *hsearch_rw_new(void);


////////
// Put an item ('key'->'val' pair) into the hash table 'hsrw'. It returns 1 if it is successfully put, or 0.
// If the key already exists in 'hsrw', it fails.
int hsearch_rw_put(struct hsearch_rw *hsrw, char *key, void *val);
int hsearch_rw_putitem(struct hsearch_rw *hsrw, struct hsearch_rw_item item);


////////
// Get the value mapped by 'key' as a void pointer from the hash table 'hsrw'. Returns NULL if 'hsrw' does not have 'key'.
void *hsearch_rw_get(struct hsearch_rw *hsrw, const char *key);


////////
// Returns 1 if 'hsrw' contains 'key' as key of hash table, or 0.
int hsearch_rw_containskey(struct hsearch_rw *hsrw, const char *key);


////////
// Returns the number of mappings in hash table 'hsrw'.
size_t hsearch_rw_size(struct hsearch_rw *hsrw);


////////
// Returns the key list of 'hsrw' as a NULL-terminated array of char pointers. Use free() when done using the list.
// The content of the list is owned by the hash table and should not be modified or freed.
char **hsearch_rw_getkeys(struct hsearch_rw *hsrw);


////////
// Returns the value list of 'hsrw' as a NULL-terminated array of void pointer-pointers. Use free() when done using the list.
// The content of the list is owned by the hash table and should not be modified or freed.
void ***hsearch_rw_getvalues(struct hsearch_rw *hsrw);


////////
// Returns the item list of 'hsrw' as a '(key, val)=(NULL, NULL)'-terminated array of hsearch_rw_item structures.
// Use free() when done using the list.
// The content of the list is owned by the hash table and should not be modified or freed.
struct hsearch_rw_item *hsearch_rw_getitems(struct hsearch_rw *hsrw);


////////
// Initializes a key iterator. Modifying the hash table after calling this function invalidates the iterator.
void hsearch_rw_iterkeys_init(struct hsearch_rw *hsrw);


////////
// Advances the key iterator and retrieves the key. Returns NULL if there are no more keys.
char *hsearch_rw_iterkeys_next(struct hsearch_rw *hsrw);


////////
// Initializes a value iterator. Modifying the hash table after calling this function invalidates the iterator.
void hsearch_rw_itervalues_init(struct hsearch_rw *hsrw);


////////
// Advances the value iterator and retrieves the value. Returns NULL if there are no more values.
void **hsearch_rw_itervalues_next(struct hsearch_rw *hsrw);


////////
// Initializes a item iterator. Modifying the hash table after calling this function invalidates the iterator.
void hsearch_rw_iteritems_init(struct hsearch_rw *hsrw);


////////
// Advances the item iterator and retrieves the item. Returns '(key, val)=(NULL, NULL)' structure if there are no more items.
struct hsearch_rw_item hsearch_rw_iteritems_next(struct hsearch_rw *hsrw);


////////
// Applies 'foreach_func' for each 'key' and 'val' pair in 'hsrw'. Arbitrary data can be passed via 'user_data'.
// You can not add/remove items while iterating over it.
void hsearch_rw_foreach(struct hsearch_rw *hsrw, void (*foreach_func)(char *key, void *val, void *user_data), void *user_data);


////////
// Removes the item 'key' and its value from the hash table 'hsrw'. It returns 1 if successfully removed, or 0.
// A 'removed' structure will be filled by the pointers of removed key and value so that you can free them, since 
// hsearch_rw_remove does not release their resources automatically. If not needed, set it NULL.
int hsearch_rw_remove(struct hsearch_rw *hsrw, const char *key, struct hsearch_rw_item *removed);


////////
// Clears all mappings from 'hsrw' and frees all keys and values using 'key_free_func' and 'val_free_func'.
// If it is not needed to free them, set NULL. Returns 1 if successfully cleared, or 0.
int hsearch_rw_clear(struct hsearch_rw *hsrw, void (*key_free_func)(char *), void (*val_free_func)(void *));


////////
// Releases all resources bound to 'hsrw' and frees all keys and values using 'key_free_func' and 'val_free_func'.
// If it is not needed to free them, set NULL.
void hsearch_rw_destroy(struct hsearch_rw *hsrw, void (*key_free_func)(char *key), void (*val_free_func)(void *val));
