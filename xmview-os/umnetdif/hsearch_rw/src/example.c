//
// hsearch_rw: A wrapper of hsearch_r coming with more practical functions.
// Copyright 2013 coronocoya.net <nshou@coronocoya.net>
//
// This program can be distributed under the terms of MIT License.
// See the file COPYING.
//

#include <stdio.h>
#include <stdlib.h>
#include "hsearch_rw.h"

char *mon[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

void foreach(char *key, void *val, void *user_data){
  printf("Foreach: %s -> %d\n", key, *(int *)val);
}

int main(void){

  int i;
  int *n;

  struct hsearch_rw *hsrw = hsearch_rw_new();

  for(i=0; i<12; i++){
    n = (int *)malloc(sizeof(int));
    *n = i + 1;
    hsearch_rw_put(hsrw, mon[i], n);
  }

  printf("Size: %zd\n", hsearch_rw_size(hsrw));

  for(i=0; i<12; i++){
    printf("Get: %s -> %d\n", mon[i], *(int *)hsearch_rw_get(hsrw, mon[i]));
  }

  if(hsearch_rw_containskey(hsrw, "Jan")){
    printf("Containskey: It contains 'Jan'.\n");
  }else{
    printf("Containskey: It does not contain 'Jan'.\n");
  }
  if(hsearch_rw_containskey(hsrw, "feb")){
    printf("Containskey: It contains 'feb'.\n");
  }else{
    printf("Containskey: It does not contain 'feb'.\n");
  }

  struct hsearch_rw_item item;
  if(hsearch_rw_remove(hsrw, "Jul", &item)){
    printf("Remove: Removed 'Jul' successfully.\n");
    free(item.val);
  }

  printf("Size: %zd\n", hsearch_rw_size(hsrw));

  hsearch_rw_foreach(hsrw, foreach, NULL);

  char **keylist = hsearch_rw_getkeys(hsrw);
  for(i=0; keylist[i] != NULL; i++){
    printf("Getkeys[%d]: %s\n", i, keylist[i]);
  }
  free(keylist);

  void ***vallist = hsearch_rw_getvalues(hsrw);
  for(i=0; vallist[i] != NULL; i++){
    printf("Getvalues[%d]: %d\n", i, *(int *)*vallist[i]);
  }
  free(vallist);

  struct hsearch_rw_item *itemlist = hsearch_rw_getitems(hsrw);
  for(i=0; itemlist[i].key != NULL && itemlist[i].val != NULL; i++){
    printf("Getitems[%d]: %s -> %d\n", i, itemlist[i].key, *(int *)itemlist[i].val);
  }
  free(itemlist);

  char *key;
  hsearch_rw_iterkeys_init(hsrw);
  while((key = hsearch_rw_iterkeys_next(hsrw)) != NULL){
    printf("Iterkeys: %s\n", key);
  }

  void **val;
  hsearch_rw_itervalues_init(hsrw);
  while((val = hsearch_rw_itervalues_next(hsrw)) != NULL){
    printf("Itervalues: %d\n", **(int **)val);
  }

  struct hsearch_rw_item itm;
  hsearch_rw_iteritems_init(hsrw);
  while(itm = hsearch_rw_iteritems_next(hsrw), itm.key != NULL && itm.val != NULL){
    printf("Iteritems: %s -> %d\n", itm.key, *(int *)itm.val);
  }

  hsearch_rw_destroy(hsrw, NULL, free);

  return 0;
}
