//
// hsearch_rw: A wrapper of hsearch_r coming with more practical functions.
// Copyright 2013 coronocoya.net <nshou@coronocoya.net>
//
// This program can be distributed under the terms of MIT License.
// See the file COPYING.
//

#define HSEARCH_RW_BASELEN 16
#define HSEARCH_RW_BUFRATE 0.75
#define HSEARCH_RW_EXTRATE 2

#define __USE_GNU
#define _GNU_SOURCE
#include <search.h>
#include <stdlib.h>
#include <string.h>
#include "hsearch_rw.h"

struct hsrw_keys{
  void *root;
  int (*compare)(const void *, const void *);
  void *data;
  size_t len;
};

struct hsrw_keys_node{
  void *val;
  struct hsrw_keys *parent;
};

static int hsrw_keys_node_compare(const void *a, const void *b){
  struct hsrw_keys *parent = ((struct hsrw_keys_node *)a)->parent;
  const void *vala = ((struct hsrw_keys_node *)a)->val;
  const void *valb = ((struct hsrw_keys_node *)b)->val;
  return (parent->compare)(vala, valb);
}

static struct hsrw_keys *hsrw_keys_new(int (*compare)(const void *, const void *)){
  struct hsrw_keys *hsrw_keys;
  hsrw_keys = (struct hsrw_keys *)malloc(sizeof(struct hsrw_keys));
  if(hsrw_keys == NULL){
    return NULL;
  }

  hsrw_keys->root = NULL;
  hsrw_keys->compare = compare;
  hsrw_keys->data = NULL;
  hsrw_keys->len = 0;

  return hsrw_keys;
}

static int hsrw_keys_add(struct hsrw_keys *hsrw_keys, void *val){
  struct hsrw_keys_node *node;
  node = (struct hsrw_keys_node *)malloc(sizeof(struct hsrw_keys_node));
  if(node == NULL){
    return 0;
  }
  node->val = val;
  node->parent = hsrw_keys;

  void *reg = tsearch((void *)node, &hsrw_keys->root, hsrw_keys_node_compare);
  if(reg == NULL || (*(struct hsrw_keys_node **)reg) != node){
    free(node);
    return 0;
  }

  hsrw_keys->len += 1;
  return 1;
}

static int hsrw_keys_contains(struct hsrw_keys *hsrw_keys, const void *val){
  struct hsrw_keys_node keynode = {
    .val = (void *)val,
    .parent = hsrw_keys
  };
  void *found = tfind((void *)&keynode, &hsrw_keys->root, hsrw_keys_node_compare);
  return found != NULL ? 1 : 0;
}

static void *hsrw_keys_delete(struct hsrw_keys *hsrw_keys, void *val){
  struct hsrw_keys_node keynode = {
    .val = val,
    .parent = hsrw_keys
  };
  void *found = tfind((void *)&keynode, &hsrw_keys->root, hsrw_keys_node_compare);
  if(found == NULL){
    return NULL;
  }

  struct hsrw_keys_node *delnode = *(struct hsrw_keys_node **)found;
  void *del = tdelete((void *)&keynode, &hsrw_keys->root, hsrw_keys_node_compare);
  if(del == NULL){
    return NULL;
  }

  void *forfree = delnode->val;
  free(delnode);
  hsrw_keys->len -= 1;
  return forfree;
}

struct hsrw_keys_foreach_storage{
  void (*foreach_func)(const void *, void *);
  void *user_data;
};

static void hsrw_keys_foreach_action(const void *nodep, const VISIT which, const int depth){
  if(which == postorder || which == leaf){
    struct hsrw_keys_node *node = *(struct hsrw_keys_node **)nodep;
    struct hsrw_keys *parent = node->parent;
    struct hsrw_keys_foreach_storage *storage = (struct hsrw_keys_foreach_storage *)parent->data;
    (storage->foreach_func)((const void *)node->val, storage->user_data);
  }
}

static void hsrw_keys_foreach(struct hsrw_keys *hsrw_keys, void (*foreach_func)(const void *, void *), void *user_data){
  struct hsrw_keys_foreach_storage storage = {
    .foreach_func = foreach_func,
    .user_data = user_data
  };
  hsrw_keys->data = &storage;
  twalk(hsrw_keys->root, hsrw_keys_foreach_action);
  hsrw_keys->data = NULL;
}

struct hsrw_keys_getat_storage{
  size_t walking;
  size_t index;
  void *data;
};

static void hsrw_keys_getat_action(const void *nodep, const VISIT which, const int depth){
  if(which == postorder || which == leaf){
    struct hsrw_keys_node *node = *(struct hsrw_keys_node **)nodep;
    struct hsrw_keys *parent = node->parent;
    struct hsrw_keys_getat_storage *storage = (struct hsrw_keys_getat_storage *)parent->data;
    if(storage->walking == storage->index){
      storage->data = (void *)(&node->val);
    }
    storage->walking += 1;
  }
}

static void **hsrw_keys_getat(struct hsrw_keys *hsrw_keys, size_t index){
  struct hsrw_keys_getat_storage storage = {
    .walking = 0,
    .index = index,
    .data = NULL,
  };
  hsrw_keys->data = &storage;
  twalk(hsrw_keys->root, hsrw_keys_getat_action);
  void **ret = (void **)NULL;
  if(storage.data != NULL){
    ret = (void **)storage.data;
  }
  hsrw_keys->data = NULL;
  return ret;
}

static void hsrw_keys_clear_free(void *_node){
  struct hsrw_keys_node *node = (struct hsrw_keys_node *)_node;
  struct hsrw_keys *parent = node->parent;
  void *val = node->val;
  ((void (*)(void *))parent->data)(val);
  free(node);
}

static void hsrw_keys_clear(struct hsrw_keys *hsrw_keys, void (*free_func)(void *)){
  hsrw_keys->data = (void *)free_func;
  tdestroy(hsrw_keys->root, hsrw_keys_clear_free);
  hsrw_keys->data = NULL;
  hsrw_keys->root = NULL;
  hsrw_keys->len = 0;
}

static void hsrw_keys_destroy(struct hsrw_keys *hsrw_keys){
  free(hsrw_keys);
}

struct hsearch_rw{
  size_t len;
  size_t cap;
  struct hsearch_data *htab;
  struct hsrw_keys *keys;
  size_t iterkeys;
  size_t itervals;
  size_t iteritems;
};

struct hsearch_rw *hsearch_rw_new(void){
  struct hsearch_rw *hsrw;
  hsrw = (struct hsearch_rw *)malloc(sizeof(struct hsearch_rw));
  if(hsrw == NULL){
    return NULL;
  }

  hsrw->len = 0;
  hsrw->cap = HSEARCH_RW_BASELEN;

  hsrw->htab = (struct hsearch_data *)malloc(sizeof(struct hsearch_data));
  if(hsrw->htab == NULL){
    free(hsrw);
    return NULL;
  }

  memset(hsrw->htab, 0, sizeof(struct hsearch_data));
  int ret = hcreate_r(HSEARCH_RW_BASELEN, hsrw->htab);
  if(ret == 0){
    free(hsrw->htab);
    free(hsrw);
    return NULL;
  }

  hsrw->keys = hsrw_keys_new((int (*)(const void *, const void *))strcmp);
  if(hsrw->keys == NULL){
    hdestroy_r(hsrw->htab);
    free(hsrw->htab);
    free(hsrw);
    return NULL;
  }

  return hsrw;
}

struct htabcpy{
  struct hsearch_data *oldhtab;
  struct hsearch_data *newhtab;
  unsigned int errors;
  char *ex;
};

static void htabcpy_func(const void *val, void *user_data){
  struct htabcpy *htabcpy = (struct htabcpy *)user_data;
  char *key = (char *)val;

  if(htabcpy->ex != NULL && strcmp(key, htabcpy->ex) == 0){
    return;
  }

  int ret;
  ENTRY *retval;
  ENTRY e = {.key = key};

  ret = hsearch_r(e, FIND, &retval, htabcpy->oldhtab);
  if(ret == 0){
    htabcpy->errors += 1;
    return;
  }

  e.data = retval->data;
  ret = hsearch_r(e, ENTER, &retval, htabcpy->newhtab);
  if(ret == 0){
    htabcpy->errors += 1;
    return;
  }
}

int hsearch_rw_put(struct hsearch_rw *hsrw, char *key, void *val){
  if(hsrw == NULL || key ==  NULL){
    return 0;
  }

  if(hsrw_keys_contains(hsrw->keys, (void *)key)){
    return 0;
  }

  int ret;
  if(hsrw->len > (size_t)(hsrw->cap * HSEARCH_RW_BUFRATE)){
    size_t newcap = hsrw->cap * HSEARCH_RW_EXTRATE;
    struct hsearch_data *newhtab;
    newhtab = (struct hsearch_data *)malloc(sizeof(struct hsearch_data));
    if(newhtab != NULL){
      memset(newhtab, 0, sizeof(struct hsearch_data));
      ret = hcreate_r(newcap, newhtab);
      if(ret != 0){
        struct htabcpy htabcpy = {
          .oldhtab = hsrw->htab,
          .newhtab = newhtab,
          .errors = 0,
          .ex = NULL,
        };
        hsrw_keys_foreach(hsrw->keys, htabcpy_func, &htabcpy);
        if(htabcpy.errors == 0){
          hdestroy_r(hsrw->htab);
          free(hsrw->htab);
          hsrw->htab = newhtab;
          hsrw->cap = newcap;
        }else{
          hdestroy_r(newhtab);
          free(newhtab);
        }
      }else{
        free(newhtab);
      }
    }
  }

  ret = hsrw_keys_add(hsrw->keys, (void *)key);
  if(!ret){
    return 0;
  }

  ENTRY ent = {.key = key, .data = val};
  ENTRY *retval;
  ret = hsearch_r(ent, ENTER, &retval, hsrw->htab);
  if(ret == 0){
    hsrw_keys_delete(hsrw->keys, (void *)key);
    return 0;
  }

  hsrw->len += 1;
  return 1;
}

int hsearch_rw_putitem(struct hsearch_rw *hsrw, struct hsearch_rw_item item){
  return hsearch_rw_put(hsrw, item.key, item.val);
}

void *hsearch_rw_get(struct hsearch_rw *hsrw, const char *key){
  if(hsrw == NULL || key == NULL){
    return NULL;
  }

  int ret;
  ENTRY *retval;
  ENTRY e = {.key = (char *)key};
  ret = hsearch_r(e, FIND, &retval, hsrw->htab);
  return ret != 0 ? retval->data : NULL;
}

int hsearch_rw_containskey(struct hsearch_rw *hsrw, const char *key){
  if(hsrw == NULL || key == NULL){
    return 0;
  }
  return hsrw_keys_contains(hsrw->keys, (const void *)key);
}

size_t hsearch_rw_size(struct hsearch_rw *hsrw){
  if(hsrw == NULL){
    return 0;
  }
  return hsrw->len;
}

struct getkeys{
  size_t index;
  char **keylist;
};

static void getkeys_func(const void *val, void *user_data){
  struct getkeys *getkeys = (struct getkeys *)user_data;
  char *key = (char *)val;
  getkeys->keylist[getkeys->index] = key;
  getkeys->index += 1;
}

char **hsearch_rw_getkeys(struct hsearch_rw *hsrw){
  if(hsrw == NULL){
    return NULL;
  }

  char **keylist = (char **)malloc(sizeof(char *) * (hsrw->len + 1));
  if(keylist == NULL){
    return NULL;
  }

  struct getkeys getkeys = {
    .index = 0,
    .keylist = keylist,
  };
  hsrw_keys_foreach(hsrw->keys, getkeys_func, &getkeys);

  keylist[getkeys.index] = NULL;
  return keylist;
}

struct getvals{
  size_t index;
  void ***vallist;
  struct hsearch_data *htab;
};

static void getvals_func(const void *val, void *user_data){
  struct getvals *getvals = (struct getvals *)user_data;
  char *key = (char *)val;

  int ret;
  ENTRY *retval;
  ENTRY e = {.key = key};
  ret = hsearch_r(e, FIND, &retval, getvals->htab);

  if(ret != 0){
    getvals->vallist[getvals->index] = &(retval->data);
  }else{
    getvals->vallist[getvals->index] = NULL;
  }

  getvals->index += 1;
}

void ***hsearch_rw_getvalues(struct hsearch_rw *hsrw){
  if(hsrw == NULL){
    return NULL;
  }

  void ***vallist = (void ***)malloc(sizeof(void **) * (hsrw->len + 1));
  if(vallist == NULL){
    return NULL;
  }

  struct getvals getvals = {
    .index = 0,
    .vallist = vallist,
    .htab = hsrw->htab,
  };
  hsrw_keys_foreach(hsrw->keys, getvals_func, &getvals);

  vallist[getvals.index] = NULL;
  return vallist;
}

struct getitems{
  size_t index;
  struct hsearch_rw_item *itemlist;
  struct hsearch_data *htab;
};

static void getitems_func(const void *val, void *user_data){
  struct getitems * getitems = (struct getitems *)user_data;
  char *key = (char *)val;

  int ret;
  ENTRY *retval;
  ENTRY e = {.key = key};
  ret = hsearch_r(e, FIND, &retval, getitems->htab);

  if(ret != 0){
    getitems->itemlist[getitems->index].key = key;
    getitems->itemlist[getitems->index].val = retval->data;
  }else{
    getitems->itemlist[getitems->index].key = NULL;
    getitems->itemlist[getitems->index].val = NULL;
  }

  getitems->index += 1;
}

struct hsearch_rw_item *hsearch_rw_getitems(struct hsearch_rw *hsrw){
  if(hsrw == NULL){
    return NULL;
  }

  struct hsearch_rw_item *itemlist = (struct hsearch_rw_item *)malloc(sizeof(struct hsearch_rw_item) * (hsrw->len + 1));
  if(itemlist == NULL){
    return NULL;
  }

  struct getitems getitems = {
    .index = 0,
    .itemlist = itemlist,
    .htab = hsrw->htab,
  };
  hsrw_keys_foreach(hsrw->keys, getitems_func, &getitems);

  itemlist[getitems.index].key = NULL;
  itemlist[getitems.index].val = NULL;

  return itemlist;
}

void hsearch_rw_iterkeys_init(struct hsearch_rw *hsrw){
  if(hsrw != NULL){
    hsrw->iterkeys = 0;
  }
}

char *hsearch_rw_iterkeys_next(struct hsearch_rw *hsrw){
  if(hsrw == NULL){
    return NULL;
  }

  char **key = (char **)hsrw_keys_getat(hsrw->keys, hsrw->iterkeys);
  if(key != NULL){
    hsrw->iterkeys += 1;
    return *key;
  }else{
    return NULL;
  }
}

void hsearch_rw_itervalues_init(struct hsearch_rw *hsrw){
  if(hsrw != NULL){
    hsrw->itervals = 0;
  }
}

void **hsearch_rw_itervalues_next(struct hsearch_rw *hsrw){
  if(hsrw == NULL){
    return NULL;
  }

  char **key = (char **)hsrw_keys_getat(hsrw->keys, hsrw->itervals);
  if(key != NULL){
    int ret;
    ENTRY *retval;
    ENTRY e = {.key = *key};
    ret = hsearch_r(e, FIND, &retval, hsrw->htab);
    if(ret != 0){
      hsrw->itervals += 1;
      return &(retval->data);
    }else{
      return NULL;
    }
  }else{
    return NULL;
  }
}

void hsearch_rw_iteritems_init(struct hsearch_rw *hsrw){
  if(hsrw != NULL){
    hsrw->iteritems = 0;
  }
}

struct hsearch_rw_item hsearch_rw_iteritems_next(struct hsearch_rw *hsrw){
  struct hsearch_rw_item nullitem = {
    .key = NULL,
    .val = NULL,
  };

  if(hsrw == NULL){
    return nullitem;
  }

  char **key = (char **)hsrw_keys_getat(hsrw->keys, hsrw->iteritems);
  if(key != NULL){
    int ret;
    ENTRY *retval;
    ENTRY e = {.key = *key};
    ret = hsearch_r(e, FIND, &retval, hsrw->htab);
    if(ret != 0){
      struct hsearch_rw_item item = {
        .key = *key,
        .val = retval->data,
      };
      hsrw->iteritems += 1;
      return item;
    }else{
      return nullitem;
    }
  }else{
    return nullitem;
  }
}

struct foreachkeys{
  struct hsearch_data *htab;
  void (*foreach_func)(char *, void *, void *);
  void *user_data;
};

static void foreachkeys_func(const void *val, void *user_data){
  struct foreachkeys *foreachkeys = (struct foreachkeys *)user_data;
  char *key = (char *)val;
  int ret;
  ENTRY *retval;
  ENTRY e = {.key = key};

  ret = hsearch_r(e, FIND, &retval, foreachkeys->htab);
  if(ret != 0){
    (foreachkeys->foreach_func)(key, retval->data, foreachkeys->user_data);
  }
}

void hsearch_rw_foreach(struct hsearch_rw *hsrw, void (*foreach_func)(char *, void *, void *), void *user_data){
  if(hsrw == NULL || foreach_func == NULL){
    return;
  }

  struct foreachkeys foreachkeys = {
    .htab = hsrw->htab,
    .foreach_func = foreach_func,
    .user_data = user_data,
  };

  hsrw_keys_foreach(hsrw->keys, foreachkeys_func, &foreachkeys);
}

int hsearch_rw_remove(struct hsearch_rw *hsrw, const char *key, struct hsearch_rw_item *removed){
  if(removed != NULL){
    removed->key = NULL;
    removed->val = NULL;
  }

  if(hsrw == NULL || key == NULL){
    return 0;
  }

  if(!hsrw_keys_contains(hsrw->keys, (void *)key)){
    return 0;
  }

  int ret;
  ENTRY *retval;
  ENTRY e = {.key = (char *)key};
  ret = hsearch_r(e, FIND, &retval, hsrw->htab);
  if(ret == 0){
    return 0;
  }

  size_t newcap = hsrw->cap;
  if(hsrw->len - 1 < (size_t)(hsrw->cap / HSEARCH_RW_EXTRATE * HSEARCH_RW_BUFRATE) && hsrw->cap / HSEARCH_RW_EXTRATE >= HSEARCH_RW_BASELEN){
    newcap = hsrw->cap / HSEARCH_RW_EXTRATE;
  }

  struct hsearch_data *newhtab;
  newhtab = (struct hsearch_data *)malloc(sizeof(struct hsearch_data));
  if(newhtab == NULL){
    return 0;
  }

  memset(newhtab, 0, sizeof(struct hsearch_data));
  ret = hcreate_r(newcap, newhtab);
  if(ret == 0){
    free(newhtab);
    return 0;
  }

  struct htabcpy htabcpy = {
    .oldhtab = hsrw->htab,
    .newhtab = newhtab,
    .errors = 0,
    .ex = (char *)key,
  };

  hsrw_keys_foreach(hsrw->keys, htabcpy_func, &htabcpy);
  if(htabcpy.errors != 0){
    hdestroy_r(newhtab);
    free(newhtab);
    return 0;
  }

  if(removed != NULL){
    removed->key = (char *)hsrw_keys_delete(hsrw->keys, (void *)key);
    removed->val = retval->data;
  }

  hdestroy_r(hsrw->htab);
  free(hsrw->htab);
  hsrw->htab = newhtab;
  hsrw->cap = newcap;
  hsrw->len -= 1;

  return 1;
}

struct clear_val_storage{
  void (*val_free_func)(void *);
  struct hsearch_rw *hsrw;
};

static void clear_val_func(const void *val, void *user_data){
  struct clear_val_storage *storage = (struct clear_val_storage *)user_data;
  struct hsearch_rw *hsrw = storage->hsrw;
  char *key = (char *)val;
  int ret;
  ENTRY *retval;
  ENTRY e = {.key = key};

  ret = hsearch_r(e, FIND, &retval, hsrw->htab);
  if(ret != 0){
    (storage->val_free_func)(retval->data);
  }
}

static void clear_key_do_nothing(void *val){
  ;
}

int hsearch_rw_clear(struct hsearch_rw *hsrw, void (*key_free_func)(char *), void (*val_free_func)(void *)){
  if(hsrw == NULL){
    return 0;
  }

  if(hsrw->len == 0){
    return 1;
  }

  struct hsearch_data *newhtab;
  newhtab = (struct hsearch_data *)malloc(sizeof(struct hsearch_data));
  if(newhtab == NULL){
    return 0;
  }

  memset(newhtab, 0, sizeof(struct hsearch_data));
  int ret = hcreate_r(HSEARCH_RW_BASELEN, newhtab);
  if(ret == 0){
    free(newhtab);
    return 0;
  }

  if(val_free_func != NULL){
    struct clear_val_storage storage = {
      .val_free_func = val_free_func,
      .hsrw = hsrw
    };
    hsrw_keys_foreach(hsrw->keys, clear_val_func, &storage);
  }

  if(key_free_func != NULL){
    hsrw_keys_clear(hsrw->keys, (void (*)(void *))key_free_func);
  }else{
    hsrw_keys_clear(hsrw->keys, clear_key_do_nothing);
  }

  hdestroy_r(hsrw->htab);
  free(hsrw->htab);
  hsrw->htab = newhtab;
  hsrw->len = 0;
  hsrw->cap = HSEARCH_RW_BASELEN;
  return 1;
}

void hsearch_rw_destroy(struct hsearch_rw *hsrw, void (*key_free_func)(char *), void (*val_free_func)(void *)){
  if(hsrw != NULL){
    if(val_free_func != NULL){
      struct clear_val_storage storage = {
        .val_free_func = val_free_func,
        .hsrw = hsrw
      };
      hsrw_keys_foreach(hsrw->keys, clear_val_func, &storage);
    }

    if(key_free_func != NULL){
      hsrw_keys_clear(hsrw->keys, (void (*)(void *))key_free_func);
    }else{
      hsrw_keys_clear(hsrw->keys, clear_key_do_nothing);
    }
    hsrw_keys_destroy(hsrw->keys);
    hdestroy_r(hsrw->htab);
    free(hsrw->htab);
    free(hsrw);
  }
}
