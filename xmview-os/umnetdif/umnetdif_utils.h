/*   Copyright 2014 Raffaele Lovino <raff.lovino@gmail.com>
 *   
 *   This is part of umNETDIF
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License, version 2, as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
 *
 */

#ifndef _UMNETDIF_UTILS_H
#define _UMNETDIF_UTILS_H



/*translate numeric fd into string*/
char *int_to_string(int fd);

/*get virtual interface IPv4 address as struct in_addr*/
/*this is a wrapper! in future it will provide a way to determine the virtual if ip addr,
but for now it simply returns the hardcoded 10.0.6.66 addr*/
int get_virt_if_ipv4_addr(struct in_addr *inp);

/*get ipv4 decimal dotted string address related to any kind of network interface*/
void get_interface_ipv4_addr(char *ifname, char *ipv4_addr_ptr);


/*get random port number to be tested later*/
uint16_t get_rand_port();

/*check whether 'port' is free for socket 'type' using interface with address 'addr'*/
int port_is_free(uint32_t addr, uint16_t port, int type);



#endif
